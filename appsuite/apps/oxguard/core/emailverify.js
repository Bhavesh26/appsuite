define("oxguard/core/emailverify",["gettext!oxguard"],function(a){"use strict";function b(a){return a.match(/[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[^\s\.]*)?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i)}function c(c,d){$(c).on("input",function(){b($(c).val())?($(c).removeClass("oxguard_badpass"),d&&$(d).html("")):($(c).addClass("oxguard_badpass"),d&&$(d).html(a("Not valid email")))})}function d(b,c,d){$(c).on("input",function(){$(b).val()===$(c).val()?($(c).removeClass("oxguard_badpass"),d&&$(d).html("")):($(c).addClass("oxguard_badpass"),d&&$(d).html(a("Emails not equal")))})}return{validate:b,autoCompare:d,setValidate:c}});
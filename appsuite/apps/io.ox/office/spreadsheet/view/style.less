/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016 OX Software GmbH
 * Mail: info@open-xchange.com
 *
 * @author Daniel Rentz <daniel.rentz@open-xchange.com>
 */

@import "../../tk/definitions";

/* ========================================================================= */
/* spreadsheet application                                                   */
/* ========================================================================= */

/* modify the color of the application icon in the top launcher */
//#io-ox-topbar .launcher.active-app.user-content.spreadsheet-launcher i {
//    color: #3c6;
//}

/* restrict all definitions to the OX Spreadsheet application */
.io-ox-office-main.io-ox-office-spreadsheet-main {

/* opacity for all kinds of overlay background nodes */
@overlay-opacity: 0.03;
/* fill color for overlay background nodes (transparent black) */
@overlay-fill-color: fade(black, percentage(@overlay-opacity));

/* selection color for cells and drawing objects in inactive panes */
@inactive-selection-color: lighten(greyscale(@doc-selection-color), 15%);
/* special marker color for collapsed columns/rows */
@collapsed-color: #ff9999;

/* z-index of marker nodes for hidden columns/rows rendered above the header cell nodes */
@header-marker-z-index: 1;
/* z-index of resizer nodes rendered above the header cell nodes */
@header-resizer-z-index: 2;

/* z-index of overlay ranges for remote selections */
@remote-z-index: 7;
/* z-index of the selection elements rendered above the cells */
@selection-z-index: 11;
/* z-index of the form controls rendered above the selection */
@form-z-index: 12;
/* z-index of the highlighted ranges rendered above the selection (but below drawings) */
@highlight-fill-z-index: 15;

/* drawing layer reserves z-indexes 30-49 */

/* z-index of the active range rendered when selecting range in cell edit mode */
@active-range-z-index: 50;
/* z-index of the highlighted range border handles rendered above the highlight fill area */
@highlight-border-z-index: 55;
/* z-index of the highlighted range resize handles rendered above the highlight borders */
@highlight-resizer-z-index: 56;

/* z-index of tooltip nodes (do not hide the text input control) */
@tooltip-z-index: 60;
/* z-index of the text input control rendered above the cells */
@input-z-index: 70;
/* z-index of the overlay nodes used while tracking column/row size, and split tracking */
@overlay-z-index: 100;

@light-shadow-effect: 0 0 5px fade(black, 20%);
@strong-shadow-effect: 0 0 10px fade(black, 20%);

/* special mouse cursor for cells similar to desktop spreadsheet applications */
.cell-cursor() {
    cursor: default;
    cursor: cell;
}

/* default formatting of all cells, and the inline text area */
.default-cell-formats() {
    font-family: Arial,sans-serif;
    font-size: 11pt;
    font-style: normal;
    font-weight: normal;
    color: black;
    text-decoration: none;
    background-color: transparent;
}

/* ========================================================================= */
/* header panes and grid panes                                               */
/* ========================================================================= */

.pane-root {
    overflow: hidden;

    /* header panes (column/row headers) =================================== */

    /* general styling for header panes and corner pane */
    > .header-pane, > .corner-pane {
        position: absolute;
        overflow: hidden;
        .user-select(none);
        background-color: @shaded-pane-fill-color;
        .light-heading-font(9pt);
        text-align: center;
        vertical-align: middle;
        line-height: normal;
        text-shadow: 0 0 3px fade(black, 20%);

        .cell {
            position: absolute;
            .box-sizing(border-box);
            overflow: hidden;
        }
    }

    /* mouse pointer and hover effects for header panes and corner pane */
    > .header-pane .cell, .corner-pane {
        cursor: pointer;
        &:hover::after {
            content: "";
            .absolute-with-distance(0);
            background-color: fade(black, 5%);
        }
    }

    /* specific styling for header panes */
    > .header-pane {

        /* marker formatting for hidden columns/rows */
        @marker-border-width: 8px;
        @marker-border-color: @collapsed-color;
        @marker-fill-color: mix(@collapsed-color, @shaded-pane-fill-color, 40%);

        /* formatting for touch resizers for column/row size */
        @touchresizer-size: 16px;
        @touchresizer-padding: 5px;

        .focus-outline(none);

        .header-content {
            position: absolute;
            box-shadow: @light-shadow-effect;
        }

        .marker {
            &, div {
                position: absolute;
                z-index: @header-marker-z-index;
                width: 0;
                height: 0;
            }

            > div {
                left: (-@marker-border-width);
                top: (-@marker-border-width);
                border: @marker-border-width solid transparent;
            }

            > div > div {
                left: (-@marker-border-width + 1);
                top: (-@marker-border-width + 1);
                border: (@marker-border-width - 1) solid transparent;
            }
        }

        .resizer {
            position: absolute;
            z-index: @header-resizer-z-index;
            .transition(background-color 0.2s linear);

            &:hover {
                background-color: fade(@doc-selection-color, 50%);
            }

            &.collapsed:hover {
                background-color: fade(@collapsed-color, 50%);
            }
        }

        .touchresizer {
            position: absolute;
            width: (@touchresizer-size + 2 * @touchresizer-padding);
            height: (@touchresizer-size + 2 * @touchresizer-padding);
            z-index: @header-resizer-z-index;

            > i {
                position: absolute;
                left: @touchresizer-padding;
                top: @touchresizer-padding;
                width: @touchresizer-size;
                height: @touchresizer-size;
                .box-sizing(border-box);
                background-color: white;
                border: 1px solid @doc-selection-color;
                border-radius: 50%;
                box-shadow: 0 0 0 1px fade(white, 50%);
                color: @doc-selection-color;
                font-size: 10px;
                line-height: (@touchresizer-size - 2px);
            }
        }

        &[data-orientation="columns"] {
            top: 0;

            .header-content, .cell, .resizer {
                top: 0;
                bottom: 0;
            }

            .cell {
                border-right: @standard-border;
                border-bottom: @standard-border;
            }

            .cell.prev-hidden {
                border-left: @standard-border;
            }

            .marker {
                top: @marker-border-width;
                > div {
                    border-top-color: @marker-border-color;
                    > div {
                        top: (-@marker-border-width);
                        border-top-color: @marker-fill-color;
                    }
                }
            }

            .resizer, .touchresizer {
                cursor: ew-resize;
            }

            .touchresizer {
                top: (2px - @touchresizer-padding);
                margin-left: (-(@touchresizer-size / 2) - @touchresizer-padding);
            }
        }

        &[data-orientation="rows"] {
            left: 0;

            .header-content, .cell, .resizer {
                left: 0;
                right: 0;
            }

            .cell {
                border-right: @standard-border;
                border-bottom: @standard-border;
            }

            .cell.prev-hidden {
                border-top: @standard-border;
            }

            .marker {
                left: @marker-border-width;
                > div {
                    border-left-color: @marker-border-color;
                    > div {
                        left: (-@marker-border-width);
                        border-left-color: @marker-fill-color;
                    }
                }
            }

            .resizer, .touchresizer {
                cursor: ns-resize;
            }

            .touchresizer {
                left: (2px - @touchresizer-padding);
                margin-top: (-(@touchresizer-size / 2) - @touchresizer-padding);
            }
        }
    }

    /* hide resizer nodes in read-only mode, if sheet is protected, or in cell edit mode */
    &:not(.edit-mode), &.sheet-locked, &.cell-edit {
        > .header-pane .resizer {
            display: none;
        }
    }

    /* hide resizer nodes while resize tracking is active (make transparent instead
        of really hiding to prevent focus problems in Firefox while tracking) */
    &.tracking-active > .header-pane .resizer:hover {
        background-color: transparent;
    }

    /* selection highlighting in header cells (not while drawing selection is active) */
    &:not(.drawing-selection) > .header-pane {

        .cell.selected {
            font-weight: bold;
        }

        .selection-styles(@selection-color) {
            .cell.selected {
                background-color: fade(@selection-color, 40%);
            }
            &[data-orientation="columns"] .cell.selected {
                border-bottom-color: fade(@selection-color, 70%);
            }
            &[data-orientation="rows"] .cell.selected {
                border-right-color: fade(@selection-color, 70%);
            }
        }
        &:not(.focused) { .selection-styles(@inactive-selection-color); }
        &.focused { .selection-styles(@doc-selection-color); }
    }

    /* specific styling for corner pane */
    > .corner-pane {
        .box-sizing(content-box);
        left: 0;
        top: 0;
        border-right: @standard-border;
        border-bottom: @standard-border;

        .cell {
            display: inline;
        }
    }

    /* grid panes (cells, selection, drawings) ============================= */

    > .grid-pane {
        overflow: hidden;
        .user-select(none);
        .focus-outline(none);
        background-color: @shaded-pane-fill-color;

        .grid-scroll {
            .absolute-with-distance(0);
            .overflow(scroll);
            z-index: 1; /* bug 36003: No scroll bar in split views */
            font-size: 0; /* prevent that browser enlarges scroll area by a few pixels */

            .grid-scroll-size {
                position: relative;
                display: inline-block;
                overflow: hidden;
                cursor: cell;
            }
        }

        .grid-layer-root {
            position: absolute;
            overflow: visible;
            left: 0;
            top: 0;
            background-color: white;
            box-shadow: @light-shadow-effect;
            .cell-cursor();
        }

        .grid-layer {
            position: absolute;
            left: 0;
            top: 0;
            width: 1px;
            height: 1px;
            overflow: visible;
        }

        /* cell renderer (cells and grid lines) ---------------------------- */

        /* canvas elements of background layer and text layer */
        .grid-layer-root > canvas.cell-layer {
            position: absolute;
            left: 0;
            top: 0;
        }

        /* outlines of table ranges */
        .table-layer > .range {
            position: absolute;
            .box-sizing(border-box);
            border: 1px solid fade(#0000ff, 20%);

            &:hover {
                border: 1px solid fade(#0000ff, 30%);
                box-shadow: 0 0 5px fade(#0000ff, 20%);
            }
        }

        /* selection layer (cell selection) -------------------------------- */

        .selection-layer {

            > .range {
                position: absolute;
                .box-sizing(border-box);
                z-index: @selection-z-index;
                box-shadow: @light-shadow-effect;

                &.active {
                    box-shadow: @strong-shadow-effect;
                }

                > .border {
                    .absolute-with-distance(0);
                    border: 1px solid transparent;
                }

                > .border::before {
                    content: '';
                    .absolute-with-distance(0);
                    border: 1px solid fade(white, 50%);
                }

                &.active > .border {
                    .absolute-with-distance(-2px);
                    border-width: 3px;
                }

                > .fill {
                    position: absolute;
                }

                /* auto-fill tracking handles for entire columns/rows */
                > .autofill.resizers {
                    > [data-pos="r"] { margin-top: 0 !important; }
                    > [data-pos="b"] { margin-left: 0 !important; }
                }
            }

            > .active-cell {
                position: absolute;
                .box-sizing(border-box);
                z-index: @selection-z-index;

                &::before {
                    content: '';
                    .absolute-with-distance(0);
                    border: 1px solid transparent;
                }
            }
        }

        .cell-selection-styles(@selection-color; @collapsed-color) {

            .selection-layer {

                /* colors for range nodes */
                > .range {

                    /* range borders, always with passed selection color */
                    > .border { border-color: fade(@selection-color, 70%); }
                    &.tracking-active > .border { box-shadow: 0 0 3px @selection-color, 0 0 2px @selection-color inset; }

                    /* fill element, color according to collapsed state */
                    .fill-colors(@color) {
                        @fill-color: fade(@color, 20%);
                        > .fill { background-color: @fill-color; }
                    }
                    .fill-colors(@selection-color);
                    &.collapsed { .fill-colors(@collapsed-color); }

                    /* selection/auto-fill tracking handles, always with passed selection color */
                    .resizer-handle-styles(7px; 3px; -1px; @selection-color; true; true; true);
                }

                /* colors for the active cell node */
                > .active-cell {
                    @border-color: fade(@selection-color, 35%);
                    &.left::before { border-left-color: @border-color; }
                    &.top::before { border-top-color: @border-color; }
                    &.right::before { border-right-color: @border-color; }
                    &.bottom::before { border-bottom-color: @border-color; }
                }
            }

            &.touch .selection-layer > .range {
                .resizer-handle-styles(17px; 5px; -2px; @selection-color; true; true; true);
            }
        }
        &.focused { .cell-selection-styles(@doc-selection-color; @collapsed-color); }
        &:not(.focused) { .cell-selection-styles(@inactive-selection-color; @inactive-selection-color); }

        /* form layer (drop-down buttons) ---------------------------------- */

        .form-layer > .cell-dropdown-button {
            position: absolute;
            .box-sizing(border-box);
            z-index: @form-z-index;
            background: @shaded-pane-fill-color;
            border: @standard-border;
            border-radius: 0;
            .focus-outline(none);
            font-size: @standard-font-size;
            text-align: center;
            cursor: pointer;

            &:hover { background: darken(@shaded-pane-fill-color, 5%); }
            &.dropdown-open { background: darken(@shaded-pane-fill-color, 10%); }
            &.dropdown-open:hover { background: darken(@shaded-pane-fill-color, 15%); }
        }

        /* drawing layer (drawing objects and selection) ------------------- */

        .drawing-layer > .drawing {
            position: absolute;

            // images keep in their frame while zooming
            > .content > .cropping-frame > img {
                width: 100%!important;
                height: 100%!important;
            }
        }

        /* to show a user name badge for the remote selection drawing */
        .drawing-layer > .remoteselection {

            /* generic formatting of the user name badge */
            &::before {
                content: attr(data-username);
                position: absolute;
                left: -2px;
                top: 0;
                bottom: auto;
                z-index: @overlay-z-index;
                opacity: 0;
                .transition(opacity 0.2s linear);
                padding: 2px 7px;
                line-height: normal;
                .light-heading-font(@standard-font-size);
                color: white;
                white-space: nowrap;
            }

            &:hover::before {
                opacity: 1;
            }

            /* set different colors (as fill color and as border color) */
            .define-scheme-color-rules(remote-range-styles; @i; @color) {
                &[data-style="@{i}"] {
                    &::before { background-color: fade(@color, 80%); }
                }
            }
            .generate-scheme-color-rules(remote-range-styles);
        }
        
        &.focused .drawing-layer .drawing > .selection {
            .border-handle-color-styles(@doc-selection-color);
            .resizer-handle-color-styles(@doc-selection-color; true);
        }
        &:not(.focused) .drawing-layer .drawing > .selection {
            .border-handle-color-styles(@inactive-selection-color);
            .resizer-handle-color-styles(@inactive-selection-color; true);
        }

        /* highlight layer (highlighted formula ranges) -------------------- */

        .highlight-layer > .range {
            position: absolute;
            box-shadow: @light-shadow-effect;

            > .fill {
                .absolute-with-distance(2px);
                z-index: @highlight-fill-z-index;
            }

            > .static-border {
                .absolute-with-distance(0);
                /* NOT @highlight-border-z-index (adjacent draggable borders must be drawn above) */
                z-index: @highlight-fill-z-index;
                border: 1px solid black;
            }

            .border-handle-styles(2px; 3px; 0; null; true; true; true);
            .resizer-handle-styles(6px; 3px; 0; null; true; true; true);
            .border-handle-z-index(@highlight-border-z-index);
            .resizer-handle-z-index(@highlight-resizer-z-index);

            /* set different colors (as fill color and as border color) */
            .define-scheme-color-rules(highlight-range-styles; @i; @color) {
                &[data-style="@{i}"] {
                    @border-color: fade(@color, 70%);
                    .border-handle-color-styles(@border-color; true; true);
                    .resizer-handle-color-styles(@color; true; true);
                    > .fill { background-color: fade(@color, 10%); }
                    > .static-border { border-color: @border-color; }
                }
            }
            .generate-scheme-color-rules(highlight-range-styles);
        }

        &.touch .highlight-layer > .range {
            .border-handle-styles(2px; 5px; 0; null; true; true; true);
            .resizer-handle-styles(12px; 5px; 0; null; true; true; true);
        }

        /* active layer (active cell range) -------------------------------- */

        .active-layer {

            > .range {
                position: absolute;
                .border-handle-styles(2px; 0; 0; white);
                .border-handle-z-index(@active-range-z-index);
            }

            .wireframe-style(@offset) {
                > .range { .border-handle-background-style(url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAIAAABvrngfAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAJUlEQVR42l3HsREAIACDQHD/nbHwbELFy69641odA46BM67OGLgDJhIGQ+H2HgAAAABJRU5ErkJggg==) repeat 0 @offset); }
            }
            &[data-frame="0"] { .wireframe-style(0px); }
            &[data-frame="1"] { .wireframe-style(1px); }
            &[data-frame="2"] { .wireframe-style(2px); }
            &[data-frame="3"] { .wireframe-style(3px); }
            &[data-frame="4"] { .wireframe-style(4px); }
            &[data-frame="5"] { .wireframe-style(5px); }
        }

        /* remote layer (remote user selections) --------------------------- */

        .remote-layer > .range {
            position: absolute;
            z-index: @remote-z-index;
            .border-handle-z-index(@highlight-border-z-index);

            /* generic formatting of the user name badge */
            &::before {
                content: attr(data-username);
                position: absolute;
                left: 0;
                bottom: 100%;
                z-index: @overlay-z-index;
                opacity: 0;
                .transition(opacity 0.2s linear);
                padding: 2px 7px;
                line-height: normal;
                .light-heading-font(@standard-font-size);
                color: white;
                white-space: nowrap;
            }

            /* bug 35604: special position for user name badge, if selection range is at top border */
            &.badge-inside::before {
                top: 0;
                bottom: auto;
            }

            &:hover::before {
                opacity: 1;
            }

            /* set different colors (as fill color and as border color) */
            .define-scheme-color-rules(remote-range-styles; @i; @color) {
                &[data-style="@{i}"] {
                    background-color: fade(@color, 10%);
                    .border-handle-styles(1px; 0; 0; fade(@color, 50%));
                    &::before { background-color: fade(@color, 80%); }
                }
            }
            .generate-scheme-color-rules(remote-range-styles);
        }
        
        /* To show the remote selection border for a drawing */
        .remote-layer > .border {
            z-index: 34; 
            pointer-events: none;
            position: absolute;
            
            /* set different colors (as fill color and as border color) */
            .define-scheme-color-rules(remote-range-styles; @i; @color) {
                &[data-style="@{i}"] {
                    border: 2px solid @color;
                }
            }
            .generate-scheme-color-rules(remote-range-styles);
        }        

        /*
         * Bug 34978 Safari on IOS 8: If the clipboard node is out of the visible
         * window (top: 110%) the focus gets lost. Don't know why, but then there
         * is a great area which is not clickable. This behavior is reproducible
         * but not really explainable.
         */
        &.browser-safari.touch .clipboard {
            top: 0px;
        }
    }

    /* hide auto-fill handle in read-only mode */
    &:not(.edit-mode) > .grid-pane .selection-layer .autofill.resizers {
        display: none;
    }

    /* do not show auto-fill/touch-selection handles while cell in-place edit mode is active */
    &.cell-edit > .grid-pane .selection-layer .resizers {
        display: none;
    }

    /* do not show remote selections while cell in-place edit mode is active */
    &.cell-edit > .grid-pane .remote-layer {
        display: none;
    }

    /* hide cell selection and form layer while drawing selection is active */
    &.drawing-selection > .grid-pane {
        .selection-layer, .form-layer {
            display: none;
        }
    }

    /* hide form layer in read-only mode */
    &:not(.edit-mode) > .grid-pane .form-layer {
        display: none;
    }

    /* show move cursor over unselected drawing frames */
    &.edit-mode:not(.sheet-locked) > .grid-pane .drawing-layer > .drawing:not(.selected) {
        cursor: move;
    }

    /* column/row resize tracking ========================================== */

    .resize-tracking {

        > * {
            display: none;
            .box-sizing(border-box);
            border: 2px fade(@doc-selection-color, 50%) none;
        }

        &[data-orientation="columns"] {
            > * { top: 0; bottom: 0; }
            > .leading { left: -10px; border-right-style: solid; }
            > .trailing { right: -10px; border-left-style: solid; }
        }

        &[data-orientation="rows"] {
            > * { left: 0; right: 0; }
            > .leading { top: -10px; border-bottom-style: solid; }
            > .trailing { bottom: -10px; border-top-style: solid; }
        }

        &.collapsed > * {
            border-color: fade(@collapsed-color, 50%);
        }
    }

    &.tracking-active .resize-tracking > * {
        display: block;
        z-index: @overlay-z-index;
        .transition(background-color 0.2s linear);
        background-color: @overlay-fill-color;
    }

    /* pane splits and tracking ============================================ */

    > .split-line {
        position: absolute;
        background-color: #181818;

        &.columns {
            top: 0;
            bottom: 0;
        }

        &.rows {
            left: 0;
            right: 0;
        }
    }

    > .split-tracking {
        .box-sizing(content-box);
        z-index: @overlay-z-index;
        .focus-outline(none);

        &.columns { cursor: ew-resize; }
        &.rows { cursor: ns-resize; }
        &.columns.rows { cursor: all-scroll; }

        &::after {
            content: '';
            display: block;
            width: 100%;
            height: 100%;
        }

        &.tracking-active::after {
            background-color: fade(@doc-selection-color, 50%);
        }

        &.tracking-active.collapsed::after {
            background-color: fade(@collapsed-color, 50%);
        }
    }

    /* target node of touch tracking must be kept in DOM (e.g. after repainting selection) */
    .touch-tracker-hidden {
        display: none;
    }

} /* end of .pane-root */

/* in-place editing ==================================================== */

.textarea-container {
    position: absolute;
    display: inline-block;
    z-index: @input-z-index;
    background-color: white;

    > * {
        z-index: @input-z-index;
        overflow-x: hidden;
        overflow-y: auto;
        margin: 0;
        padding: 0;
        .default-cell-formats();
        /* by default, Chrome renders text in underlay <div> with kerning, but text in a <textarea> without */
        text-rendering: optimizeLegibility;
    }

    > .underlay {
        .absolute-with-distance(0);
        color: transparent;
        white-space: pre-wrap;
        word-wrap: break-word;

        > span {
            border-radius: 1px;

            .define-scheme-color-rules(span-color-styles; @i; @color) {
                &[data-style="@{i}"] {
                    background-color: fade(@color, 10%);
                    box-shadow: 0 0 0 1px fade(@color, 50%);
                }
            }
            .generate-scheme-color-rules(span-color-styles);
        }
    }

    > textarea {
        -webkit-appearance: none;
        position: relative;
        display: inline-block;
        border: none;
        border-radius: 0;
        box-shadow: 0 0 0 1px @inactive-selection-color, 0 0 8px 2px fade(@inactive-selection-color, 60%);
        .focus-outline(none);

        &:focus, &:active {
            .focus-effects(@border-as-shadow: true);
        }
    }

}

/*
  textarea lay on top of the window on small devices
  because softkeyboard is often to big
  and height is undetectable on IOS
*/
.textarea-container.ontop {
    position: absolute;
    left: 1px!important;
    right: 1px!important;

    @media @portrait {
        top: 40px!important;
        height: 28px!important;
    }

    @media @landscape {
        top: 1px!important;
        height: 40px!important;
    }

    > textarea {
        width: 100%!important;
        height: 100%!important;
        font-size: 22px!important;
    }

    >.underlay {
        font-size: 22px!important;
    }
}

/* ========================================================================= */
/* control groups                                                            */
/* ========================================================================= */

.group.format-category-group .button {
    .light-heading-font(15px; bold);
    text-align: center;
}

.group.active-sheet-group {

    .button {

        &.hidden {
            display: none;
        }

        &.option-button {
            padding-left: @standard-distance-h;
            padding-right: @standard-distance-h;
            text-align: center;

            &.selected {
                background-color: white !important;
                cursor: default;

                &:not(:focus), &:active {
                    border-color: @standard-border-color !important;
                }

                &::after {
                    content: '';
                    position: absolute;
                    left: 0;
                    right: 0;
                    bottom: 0;
                    height: 4px;
                    background-color: @text-color;
                }
            }

            &.tracking-active {
                border-color: @standard-border-color !important;
            }
        }

        &.scroll-button {
            &:first-child { margin-right: 5px; }
            &:last-child { margin-left: 5px; }
        }
    }
}

/* ========================================================================= */
/* status pane                                                               */
/* ========================================================================= */

.view-pane.status-pane {

    .view-component.toolbar {
        margin-top: 0 !important;
        margin-bottom: @standard-distance-v !important;

        /* tab layout, attached to the upper border of the tool bar */
        .group .button {
            position: relative;
            top: -1px;
            border-radius: 0 0 @control-standard-border-radius @control-standard-border-radius !important;
        }

        .formula-text {
            text-align: left;

            div.label {
                text-align: left;
            }
        }
    }
}

/* ========================================================================= */
/* debugging                                                                 */
/* ========================================================================= */

&.debug-highlight {

    .debug-badge-formatting(@data-attr; @font-size) {
        content: attr(@data-attr);
        position: absolute;
        left: 0;
        top: 0;
        z-index: 10;
        padding: 1px 3px;
        background: fade(white, 60%);
        border-radius: 3px;
        font-size: @font-size;
        color: #666;
        line-height: normal;
    }

    > .app-pane .grid-pane .clipboard {
        z-index: 100;
        left: 5px;
        right: 5px;
        top: 5px;
    }

    .cell span {
        .debug-highlight-box(#88f);
    }

    /* show click/touch area of border and resize handles */
    .borders > [data-pos], .resizers > [data-pos], .touchresizer {
        background-color: fade(red, 25%);
        outline: 1px solid fade(red, 50%);
    }

    /* show column and row indexes in header panes */
    .header-pane .cell::after { .debug-badge-formatting(data-index; 10px); }
}

/* ========================================================================= */

} /* end of .io-ox-office-spreadsheet-main { */

/* ========================================================================= */
/* GUI additions for pop-up nodes                                            */
/* ========================================================================= */

/* TODO: pop-ups should have a specific application selector (.io-ox-office-spreadsheet-main) */
.io-ox-office-main.popup-container {

    /* CellStylePicker control ============================================= */

    &.popup-menu.app-spreadsheet.style-picker.family-cell .section-container .button {
        max-width: 150px;

        > .borders { .absolute-with-distance(2px); }
        > .caption { position: relative; }
    }

    /* function signature tool-tip ========================================= */

    &.popup-tooltip.signature-tooltip {

        .func-signature {
            .light-heading-font(14px);
            color: @text-color;

            .function-name {
                margin-right: 3px;
                color: darken(@doc-selection-color, 20%);
            }

            .param-active {
                font-weight: bold;
            }
        }

        .param-help {
            margin-top: @standard-distance-v;
            padding-top: @standard-distance-v;
            border-top: 1px solid lighten(@tooltip-border-color, 5%);
        }
    }

    /* floating menu for defined names ===================================== */

    &.layer-menu.defined-names .group.named-range {
        padding: @standard-distance-v @standard-distance-h;
        cursor: default;
        .clearfix();

        > .name {
            float: left;
            margin-right: @standard-distance-h;
            max-width: 250px;

            > .text {
                display: block;
                font-weight: bold;
                float: left;
                width: 100%;
                overflow: hidden;
                text-overflow: ellipsis;
            }

            > .formula {
                display: block;
                clear: both;
                float: left;
                width: 100%;
                margin-top: @standard-distance-v;
                overflow: hidden;
                text-overflow: ellipsis;
            }
        }

        > .actions {
            float: right;

            .group a {
                padding: 0px 5px !important;
            }
        }
    }
}

/* ========================================================================= */
/* global overlays                                                           */
/* ========================================================================= */

#io-ox-office-spreadsheet-sheet-dnd-tracker {

    @border-color: fade(@text-color, 25%);
    @caret-height: 7px;

    position: absolute;
    z-index: 10000;
    font-size: (@standard-font-size - 1px);
    text-align: center;
    color: fade(@text-color, 75%);

    > .sheet-name {
        display: inline-block;
        padding: 2px @standard-distance-v;
        border: 1px solid @border-color;
        border-top: none;
        border-radius: 0 0 @control-standard-border-radius @control-standard-border-radius;
        background: fade(white, 75%);
        white-space: pre;
    }

    > .caret-icon {
        position: absolute;
        top: 100%;
        left: 50%;
        width: 0px;
        height: 0px;
        margin-left: (-(@caret-height));
        border: @caret-height solid transparent;
        border-top-color: @border-color;
    }
}

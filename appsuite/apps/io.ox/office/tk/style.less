/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016 OX Software GmbH
 * Mail: info@open-xchange.com
 *
 * @author Daniel Rentz <daniel.rentz@open-xchange.com>
 */

@import "definitions";
@import "icons/docs-icons";

/* ========================================================================= */
/* definitions for application DOM tree                                      */
/* ========================================================================= */

/* restrict all definitions to OX Documents applications */
.io-ox-office-main {

/* browser fixes and workarounds =========================================== */

/* hack for IE to expand nodes beyond the size limit of single nodes, see Utils.setContainerNodeSize() */
.ie-node-size-helper > div {
    display: inline-block;
    height: 0;
}

/* hack for IE to position nodes beyond the size limit of single nodes absolutely, see Utils.setPositionInContainerNode() */
.ie-node-position-helper {
    position: absolute;
    left: 100%;
    top: 100%;
}

/* OX Document bitmap-based icons ========================================== */

i[class^="docs-"] {
    display: inline-block;
    margin: 0;
    background-origin: content-box;
    vertical-align: middle;

    &.docs-empty.small-icon { width: 12px; }
}

.docs-icons(black);

/* control groups ========================================================== */

/* prevent built-in focus rectangle */
.button, input {
    .focus-outline(none);
}

/* prevent system pop-up when tapping and holding button on iPad/iPhone (also for buttons outside .group elements) */
.button {
    -webkit-touch-callout: none;
}

/* standard styling of placeholders in input fields */
input, textarea {

    /* helper mix-in to set all style properties (see below) */
    .placeholder-properties() {
        font-size: (@standard-font-size - 2px) !important; /* IE needs !important */
        font-style: italic;
        opacity: 0.7;
    }

    /* selectors cannot be combined in a comma-separated list (one invalid selector invalidates the entire CSS rule) */
    &::-webkit-input-placeholder { .placeholder-properties(); }
    &:-moz-placeholder { .placeholder-properties(); }
    &::-moz-placeholder { .placeholder-properties(); }
    &:-ms-input-placeholder { .placeholder-properties(); }
}

/* bug 38001: suppress spin buttons for normal numeric input fields */
input[type="number"]:not([min]):not([max]) {
    -moz-appearance: textfield;

    &::-webkit-outer-spin-button, &::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
}

.group {
    display: inline-block;
    position: relative;
    .box-sizing(border-box);

    /* hidden groups ("important" to overrule display modes from other rules) */
    &.hidden {
        display: none !important;
    }

    /* disabled groups ("important" to overrule display modes from other rules) */
    &.disabled, &.disabled *, & .disabled {
        cursor: default !important;
    }

    /* common control settings --------------------------------------------- */

    div.label, .button, input {
        display: inline-block;
        position: relative;
        .box-sizing(border-box);
        margin: 0;
        padding: 0;
        overflow: hidden;
        background: none;
        border: none;
        border-radius: 0;
        box-shadow: none;
        .transition(none);
        font-size: @standard-font-size;
        font-weight: normal;
        color: @text-color;
        text-shadow: none;
        vertical-align: top;
    }

    /* control caption elements (always span elements as children) */
    div.label, .button {

        > .caption {
            display: inline-block;
            min-width: @icon-size;
            max-width: 100%;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;

            // there are problems on touch devices with bubbling the events,
            // its more solid when only the anchor itself reacts on touch
            pointer-events: none;

            &:empty { display: none; }
            > * + * { margin-left: @caption-spacing; }
        }

        /* give standard Bootstrap icons a fixed width */
        i.fa {
            display: inline-block;
            width: 18px;
            font-size: 14px;
            text-align: center;

            &.small-icon {
                width: 12px;
                font-size: 11px;
            }
        }
    }

    /* labels -------------------------------------------------------------- */

    div.label {
        text-align: left;
        cursor: default;
    }

    /* buttons ------------------------------------------------------------- */

    .button {
        text-align: left;
        cursor: pointer;

        &:hover {
            text-decoration: none;
        }
    }

    /* text fields --------------------------------------------------------- */

    .input-wrapper {
        display: inline-block;
        .box-sizing(border-box);
        width: 150px;
        padding: 0;
        vertical-align: top;
        cursor: text;

        > input {
            width: 100%;
            height: 100%;
            /* bug 28206: IE hides cursor in right-aligned text fields */
            padding-right: 1px;
            line-height: normal;
            text-overflow: ellipsis;
            -webkit-appearance: none;
        }
    }

    &.check-group > .button.select-all {
        font-style: italic;
    }

    &.text-field {
        white-space: nowrap;
    }

    &.spin-field input {
        text-align: right;
    }

} /* end of .group */

/* other DOM contents ====================================================== */

/* standard styling of header labels in structured menus */
.header-label {
    line-height: normal;
    font-size: (@standard-font-size - 2px);
    color: #777777;
    text-align: left;
    cursor: default;
}

/* ========================================================================= */
/* pop-up nodes                                                              */
/* ========================================================================= */

&.popup-container {
    position: absolute;
    z-index: @popup-z-index;
    .box-sizing(content-box);
    margin: 0;
    padding: 0;
    overflow: visible;
    background: none;
    border: none;
    border-radius: 0;
    box-shadow: none;
    font-size: 0;
    line-height: 0;

    &.popup-text-select { .user-select(text); }
    &:not(.popup-text-select) { .user-select(none); }

    > .popup-content {
        display: inline-block;
        .box-sizing(border-box);
        .focus-outline(none);
        overflow: visible;
        font-size: @standard-font-size;
        line-height: normal;

        &.hidden {
            display: none;
        }

        > .popup-busy {
            min-width: 90px;
            min-height: 36px;
        }
    }

    /* tooltips ============================================================ */

    &.popup-tooltip {
        background-color: @tooltip-fill-color;
        border: 1px solid @tooltip-border-color;
        border-radius: @tooltip-border-radius;
        .drop-shadow(3px);

        /* ignore all pointer events, unless text selection mode is enabled */
        &:not(.popup-text-select) {
            pointer-events: none;
            cursor: default;
        }

        > .popup-content {
            padding: 5px 10px;
            box-shadow: 0px 10px 20px #f7f7f7 inset;
            font-size: (@standard-font-size - 1px);
            color: lighten(@text-color, 5%);
            word-wrap: break-word;
        }
    }

    &.bootstraptooltip {
        overflow: visible!important;
        box-shadow: none;

        > .arrow {
            position: absolute;
            width: 0;
            height: 0;
            border-style: solid;
            border-color: transparent;
            top: -12px;
            border-bottom-color: #000;
            border-width: 6px;

            &.on-top {
                top: auto;
                bottom: -12px;
                border-bottom-color: transparent;
                border-top-color: #000;
            }
        }

        > .popup-content {
            box-shadow: none;
            border-radius: 3px;
            border-width: 0;
            border: none;
            color: white;
            background-color: black;
        }
    }

    /* pop-up menus ======================================================== */

    &.popup-menu {
        background-color: white;
        border: @standard-border;
        border-radius: @popup-border-radius;
        .drop-shadow();
    }

    /* item list menus ===================================================== */

    &.popup-menu.list-menu {

        > .popup-content {
            padding: @standard-distance-v 0;
        }

        /* styles for menu section nodes */
        .section-container {
            display: block;
            position: relative;
            .box-sizing(border-box);

            /* separator line between visible sections */
            &:not(.hidden) ~ .section-container:not(.hidden) {
                padding-top: @standard-distance-v;
                margin-top: @standard-distance-v;
                border-top: @standard-border;
            }

            > .header-label {
                padding: @standard-distance-v 5px;
            }
        }

        /* item list menus in list style ----------------------------------- */

        &[data-design="list"] .section-container {

            /* stack all buttons in the section nodes */
            > * {
                display: block;
            }

            /* add button formatting for "list" design mode */
            .list-group-design();
        }

        /* item list menus in grid style ----------------------------------- */

        &[data-design="grid"] .section-container {
            padding-left: 5px;
            padding-right: 5px;

            > .grid-row {
                display: block;
                white-space: nowrap;

                > .grid-cell {
                    display: inline-block;

                    .title{
                        font-weight: bold;
                        display: block;
                        margin: 5px;
                    }
                }
            }

            /* add button formatting for "framed" design mode */
            .framed-group-design();
        }
    }

    &.popup-menu.context-menu {
        .dropdown-group {
            a {
                width: 100%;
                .caption {
                    padding-right: 24px;

                    i{
                        font-size: 10px;
                        right: 0px;
                        top: 10px;
                        position: absolute;
                    }
                }
            }
        }
    }

} /* end of &.popup-container */

} /* end of .io-ox-office-main */

/* ========================================================================= */
/* dialog boxes                                                              */
/* ========================================================================= */

.io-ox-office-dialog {

    > .modal-body > .form-group:last-child {
        margin-bottom: 0;
    }

    /* no native styling for form controls on iPad/iPhone */
    input {
        -webkit-appearance: none;
    }

    /* small space between a spin field and its trailing label */
    .spin-field > .spin-field-tail {
        margin-left: 0.25em;
    }

    /* file picker widget */
    .file-picker {

        /* select file button */
        span.btn {
            color: #333;
            background-color: #eee;
            &:hover { background-color: #d5d5d5; }
        }

        /* text field */
        input.form-control[readonly] {
            background-color: #fcfcfc;
            cursor: default;
        }
    }

    /* insert image from Drive tab */
    .drive-image-picker {

        .drive-folder-list,
        .drive-image-list {
            display: inline-block;
            height: 200px;
            margin: 0;
            overflow: auto;
            vertical-align: top;
            width: 50%;
            padding:0;
        }

        .drive-image-list {
            outline: none;
            padding-left:1em;
            li.file{
                cursor: pointer;
                line-height: 30px;
                .user-select(none);
                label {
                    margin-top: 5px;
                    margin-left: 5px;
                    float: left;
                }
                .name {
                    .ellipsis();
                    padding:0 10px;
                }
                &.selected, &.selected:hover {
                    background-color: @foldertree-selected-background;
                }
            }
            &:focus .file {
                &.selected, &.selected:hover {
                    background-color: @foldertree-selected-focus-background;
                    .name {
                        color: @foldertree-focused-label-color;
                    }
                }
            }
            &.singleselect li.file .name {
                padding-left: 0.5em;
            }
            .bg-info {
                line-height: 30px;
                padding:0 1em;
                color:lighten(#000000, 50%);
            }
        }
    }

    &.save-as-dialog.save-as-folder-list {
        .folder-tree {
            height: 230px;
            padding: 0;
            margin-bottom: 20px;
        }
    }

} /* end of .io-ox-office-dialog */

/* ========================================================================= */
/* global hidden DOM storage                                                 */
/* ========================================================================= */

#io-ox-office-temp {
    position: absolute;
    top: -100%;
    bottom: 100%;
    left: 0;
    right: 0;
    overflow: hidden;

    > .font-metrics-helper {
        position: relative;
        display: inline-block;
        width: auto;
        margin: 0;
        padding: 0;
        border: none;
        line-height: normal;
        white-space: pre;
    }
}

/* ========================================================================= */
/* debug log for Selenium tests                                              */
/* ========================================================================= */

#io-ox-office-selenium {
    position: absolute;
    top: -100%;
    bottom: 100%;
    left: 0;
    right: 0;
    overflow: hidden;
}

/* ========================================================================= */
/* other generic rules                                                       */
/* ========================================================================= */

/* generic rule for disabling text-selection */
.user-select-none {
    .user-select(none);
}

/**
 * This work is provided under the terms of the CREATIVE COMMONS PUBLIC
 * LICENSE. This work is protected by copyright and/or other applicable
 * law. Any use of the work other than as authorized under this license
 * or copyright law is prohibited.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016 OX Software GmbH
 * Mail: info@open-xchange.com
 *
 * @author Daniel Rentz <daniel.rentz@open-xchange.com>
 */

@import "../../tk/definitions";

/* ========================================================================= */
/* text processing application                                               */
/* ========================================================================= */

/* restrict all definitions to the OX Text application */
.io-ox-office-main.io-ox-office-text-main {

/* change tracking style mix-ins =========================================== */

/*change tracking: fill color for inserted table cells */
@ct-table-insert-color: #e1f2fa;

/*change tracking: fill color for removed table cells */
@ct-table-remove-color: #fce6f4;

/*change tracking: border color for inserted drawings */
@ct-drawing-insert-color: #00f2fa;

/*change tracking: border color for removed drawings */
@ct-drawing-remove-color: #ff0000;

/* color sets for change track authors */
.define-scheme-color-rules(change-track-author-colors; @i; @color) {
    &[data-change-track-modified-author="@{i}"] { outline: 1px dashed @color !important; }
    &[data-change-track-inserted-author="@{i}"] { color: @color !important; }
    &[data-change-track-removed-author="@{i}"] { color: @color !important; }
}

/* comments authors colors */
.define-scheme-color-rules(comments-author-colors; @i; @color) {
    &.comment-author-@{i} { color: @color !important; }
}

/* comments rangemarker background colors */
.define-scheme-color-rules(comments-rangemarker-colors; @i; @color) {
    &.comment-author-@{i} { background-color: @color !important; }
}

/* comments authors bubble background colors */
.define-scheme-color-rules(commentsbubble-background-colors; @i; @color) {
    &.comment-author-@{i} { background-color: fade(@color, 30%); }
}

/* color sets for comment thread left border color */
.define-scheme-color-rules(comments-border-colors; @i; @color) {
    &.comment-author-@{i} { border-left: 4px solid @color !important; }
}

/* color sets for comment thread complete border color */
.define-scheme-color-rules(comments-allborder-colors; @i; @color) {
    &.comment-thread-active-author-@{i} { border-color: @color !important; }
    &.comment-thread-hovered-author-@{i} { border-color: @color !important; }
}

/* change track custom highlight style */
.change-track-highlight-styles() {
    background-color: #caddee !important;
    color: #ffffff !important;
}

/* mix-in for selection over inactive elements, such as headers, page breaks..., but without implementation for moz browser*/
.user-select-none-without-moz() {
    -webkit-user-select: none;
    -khtml-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* editor page ============================================================= */

> .app-pane .page {
    @default-page-padding: 15mm;

    /* default page formatting */
    position: relative;
    .box-sizing(content-box);
    .focus-outline(none);
    width: (210mm - @default-page-padding);
    min-height: (297mm - @default-page-padding);
    padding: @default-page-padding;
    background-color: white;
    box-shadow: 0 0 8px fade(black, 20%);
    cursor: default;

    /* text cursor over entire page, but not in read-only mode */
    &.edit-mode {
        cursor: text;

        /* resize cursor for table cells, but not in read-only mode */
        div.cell > div.resize {
            .user-select(none);
            &.bottom { cursor: row-resize; }
            &.right { cursor: col-resize; }
        }
    }

    &.inactive-mode {
        background-color: #f7f7f7;
        .inactive-selection {
            background-color: #EFEFEF;
        }
    }

    /* change tracking: add highlighting styles for all marked elements */
    .change-track-highlight {
        .change-track-highlight-styles();
    }

    /* default paragraph formatting */
    div.p {
        position: relative;
        margin: 0;
        line-height: 0;
        word-wrap: break-word;

        &.visibleAnchor {
            & > .anchorIcon {
                display: block;
                position: absolute;
                top: 0px;
                left: -25px;
            }
        }

        /* Fix for bug 26455: misplaced text contents with first-line-indent */
        > * {
           text-indent: 0;
        }

        > div.list-label {
             display: inline-block;
             /* Fix for 30649: The label is always left aligned, even if surrounding paragraph is right aligned */
             text-align: left;

             /* Safari mobile allows to visit nodes which has contenteditable=false, therefore
                we have to use a different solution. We use -webkit-user-select: none to force
                Safari mobile to jump over the text span with the fill characters.
             */
             > span {
                -webkit-user-select: none;
                text-decoration: none!important; /* bug 37411 */
             }
        }

        > div.field {
            display: inline;
            position: relative;
            &:hover span {
                background: #bbb!important;
                opacity: 0.7;
            }
        }

        .field.sf-highlight span {
            background: #bbb!important;
            opacity: 0.7;
        }

        /* needs white-space:nowrap to have correct width
           Fix for 26899: use word-wrap: normal for IE to prevent text wrapping in tab div
        */
        > div.tab {
            display: inline-block;
            white-space: nowrap;
            word-wrap: normal;
            vertical-align: bottom;
        }

        > div.drawingplaceholder, > div.commentplaceholder, > div.hardbreak, > div.rangemarker, > div.complexfield {
            display: inline;
        }

        /* default text formatting (also for fields and list labels) */
        > span, > div > span {
            font-family: sans-serif;
            font-size: 11pt;
            line-height: normal;

            /* highlighted portions (quick search) */
            &.highlight {
                opacity: 1;
                background-color: yellow !important;
                color: black !important;
                margin-top: -1px;
                margin-bottom: -1px;
                border-top: 1px dotted #505000;
                border-bottom: 1px dotted #505000;
            }

            /* needs always a left border to prevent jumping of following drawing.
               Fix for 36619: Cut a frame - click undo:=> frame is blinking
             */
            &.simulatedCursor{
                border-left: 1px solid white;
                margin-left: -1px;
                display: inline-block;
                vertical-align: bottom !important;
                // pull drawing right, otherwise it could jump into the next line
                & + .drawing{
                    margin-right: -2px !important;
                }
            }

            /* blinking cursor simulation */
            &.doblink {
                border-left: 1px solid black;
            }

            /* supporting uppercase */
            &.uppercase {
                text-transform: uppercase;
            }

            /* supporting lowercase */
            &.lowercase {
                text-transform: lowercase;
            }

            /* change tracking: inserted text span */
            &[data-change-track-inserted=true] {
                text-decoration: underline !important;
            }

            /* change tracking: removed text span */
            &[data-change-track-removed=true] {
                text-decoration: line-through !important;
            }

            /* change tracking: inserted AND removed text span */
            &[data-change-track-inserted=true][data-change-track-removed=true] {
                text-decoration: underline line-through !important;
            }
        }

        // fix for Bug 47820
        &.selectionHighlighting {
            > span:empty:after {
                content: "\00a0";
                background-color: #3399FF !important;
                color: transparent !important;
            }
        }

        /* change tracking */
        > span, > div.inline > span, > div.drawing, > div.list-label > span {
            .generate-scheme-color-rules(change-track-author-colors);
        }

        /* add change tracking highlighting for all child spans of a paragraph */
        &.change-track-highlight > span {
            .change-track-highlight-styles();
        }
    }

    /* drawings (not always in paragraph) */
    .drawing {
        outline: none;

        &.inline {
            display: inline-block;
            vertical-align: bottom;
            position: relative;

            // Quick-Fix for BUG #40053
            .browser-safari& + span:empty {
                display: inline-block;
                width: 1px;
                height: 20px;
                margin-left: 1px;
            }
        }
        &.float {
            position: relative;
        }
        &.absolute {
            position: absolute;
        }

        /* change tracking */
        &[data-change-track-inserted=true] {
            border-bottom: 3px solid @ct-drawing-insert-color !important;
        }
        &[data-change-track-removed=true] {
            border-bottom: 3px solid @ct-drawing-remove-color !important;
        }

        /* drawing groups */
        &[data-type="group"] > .content {
            border: 1px solid transparent;

            &:hover {
                border: 1px dotted fade(black, 25%);
            }
        }

        /* no pointer events for the canvas */
        > div.content > canvas {
            pointer-events: none;
        }

        /* text frames */
        > div.content.textframecontent {

            outline: none; /* avoiding browser border around text frame (Firefox) */

            > div.textframe {
                height: 100%;
                overflow: hidden;
                position: relative;
                padding-bottom: 2px; /* required to make spell check line visible */
                outline: none; /* avoiding browser border around text frame (Chrome) */
            }

        }

        /* no dotted line for text frame contents that are in comments */
        &:not(.comment):not(.selected)[data-type="shape"]:hover::before{
            content: "" !important;
            display: block !important;
            .absolute-with-distance(0);
            border: 1px dotted fade(black, 25%);
            z-index: 100;
            pointer-events: none;
        }

        /* comments */
        &.comment {

            &.childcomment {
                left: 20px;
                width: 274px;
            }

            > div.commentmetainfo {
                font-size: 10px;
                cursor: default;
                text-align: left;
                position: relative;
                padding: 10px 10px 0px 10px;

                > .commentpicture {
                    display: table-cell;
                    overflow: hidden;
                    background-size: cover;
                    background-position: center;
                    background-repeat: no-repeat;
                    border-radius: 50%;
                }

                > .commentauthordate {
                    height: 100%;
                    width: 100%;
                    vertical-align: top;
                    padding: 2px 5px;
                    display: table-cell;

                    > .commentauthor {
                        line-height: 12px;
                        max-height: 12px;
                        font-size: 8pt;
                        overflow: hidden;
                        .generate-scheme-color-rules(comments-author-colors);

                        &.halo-link:hover {
                            cursor: pointer;
                            text-decoration: underline;
                        }
                    }

                    > .commentdate  {
                        line-height: 12px;
                        max-height: 12px;
                        padding-top: 2px;
                        font-size: 7pt;
                        color: #aaa;
                    }
                }

                > .commentbutton {

                    display: none;
                    font-size: 10pt;

                    &.touch {
                        font-size: 14pt;
                    }

                    &.commentbuttonactive, &.commentbuttonhover {
                        display: table-cell;
                    }

                    > .commentcloser {
                        display: table-cell;
                        color: #000;
                        padding: 3px;
                        cursor: pointer;

                        &.touch {
                            padding-left: 10px;
                        }
                    }

                    > .commentreply {
                        display: table-cell;
                        color: #000;
                        padding: 3px;
                        cursor: pointer;
                    }
                }
            }

            > div.content {
                /* background-color: yellow; */
                padding: 5px 10px 10px 10px;
                border: none !important;
                display: block;  /* see bug #48792 */
            }

            > div.commentbottom {

                display: none;
                text-align: right;
                padding-right: 15px;
                height: 15px;
                margin-top: -20px;
                padding-bottom: 25px;

                > .shrinkarrow {
                    font-size: 12pt;
                    color: #000;
                }
            }
        }

    }

    /* setting the correct cursor over a drawing */
    &.edit-mode {

        .textframecontent {
            .drawing {
                cursor: default !important;
            }
        }

        .drawing {
            cursor: move;

            &.comment {
                cursor: text;
            }

            > .textframecontent > .textframe {
                cursor: text;
            }
        }

    }

    div.textdrawinglayer {

        /* overflow hidden for drawing outside of the page, drawing outside of the pagebreak are calculated manually in drawingstyles */
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: hidden;
        pointer-events: none;

        /* not only direct children of text drawing layer, but also drawings in groups */
        div.drawing {

            pointer-events: fill;

            &.absolute {
                position: absolute;
                z-index: 50;
            }
        }
    }

    /* the container for all comments */
    div.commentlayer {
        position: absolute;
        top: 0px;
        width: 310px;
        z-index: 100;
        cursor: default;

        &.classicview {
            /* only for testing reasons */
            width: 300px;
            background-color: #eee;
            box-shadow: 0 0 8px fade(black, 20%);
        }

        > div.commentthread {
            position: absolute;
            height: auto;
            width: 300px;
            background-color: white;
            overflow: hidden;
            border: 2px solid #fff;
            box-shadow: 0 0 8px fade(black, 20%);
            border-radius: @tooltip-border-radius;
            outline: none; /* avoiding browser border around comments (FF and IE) */
            .generate-scheme-color-rules(comments-border-colors);
            .generate-scheme-color-rules(comments-allborder-colors);

            &.avantgardview {
                /* only for testing reasons */
                box-shadow: 0 5px 10px fade(black, 20%);
            }

            &.classicview {
                /* only for testing reasons */
                box-shadow: none;
            }

            &.inactivethread {
                > div.comment.decreased > div.content > div.textframe {
                    max-height: 36px;
                    overflow: hidden;
                }

                > div.comment.decreased > div.commentbottom {
                    display: block;
                }
            }
        }

    }

    /* the container for all comment bubbles */
    div.commentbubblelayer {
        position: absolute;
        top: 0px;
        z-index: 80;

        > div.commentbubble {
            position: absolute;
            height: 25px;
            width: 25px;
            cursor: pointer;
            font-size: 18pt;

            > a > .fa {
                .generate-scheme-color-rules(comments-author-colors);
            }

            &.activated, &:hover {
                .generate-scheme-color-rules(commentsbubble-background-colors);
            }
        }

    }

    /* the container for visualizing the commented content */
    div.rangemarkeroverlay {
        position: absolute;
        width: 100%;
        left: 0px;
        top: 0px;
        z-index: 22;

        > div, > svg {
            position: absolute;
            pointer-events: none;

            &.fillcolor {
                .generate-scheme-color-rules(comments-rangemarker-colors);
            }

            &.visualizedcommenthover {
                opacity: 0.3;
            }

            &.visualizedcommentclick {
                opacity: 0.4;
            }

            &.highlightcommentlinehover {
                opacity: 0.7;
                z-index: 50;
                width: 1px;
                height: 1px;
            }

            &.highlightcommentlineclick {
                opacity: 0.7;
                z-index: 50;
                width: 2px;
                height: 2px;
            }
        }
    }

    .rangemarkeroverlay.higher-z-index {
        z-index: 42;
    }

    /* last paragraph in document */
    > div.pagecontent > div.p:last-child:after {
        content: ".";
        display: block;
        height: 0;
        clear: both;
        visibility: hidden;
    }

    /* the side bar used for change tracking */
    > div.ctsidebar {
        position: absolute;
        width: 2px;
    }

    /* the marker for the change tracked elements inside side bar used for change tracking */
    > div.ctsidebar > div.ctdiv {
        position: absolute;
        left: 0px;
        width: 2px;
        background-color: #AAA;
        z-index: 40;
    }

    /* spell checking (edit mode only) */
    &.edit-mode.spellerror-visible span.spellerror {
        /* negative margin to even out additional space from padding and border */
        margin-bottom: -2px !important;
        padding-bottom: 1px !important;
        border-bottom: 1px dotted #ff0000 !important;
    }

    /* default table formatting */
    table {
        table-layout: fixed;
        width: 100%;
        border-collapse: collapse;
        margin: 6px 0;

        &:first-child { margin-top: 0; }
        &:last-child { margin-bottom: 0; }

        table {
            margin: 0;  /* no margin for tables in tables */
        }

        tr {
            height: 100%;
        }

        td {
            height: 100%;
            padding: 4px;
            border: none; /* 1px solid transparent; Task 29289 */
            vertical-align: top;

            .generate-scheme-color-rules(change-track-author-colors);

            > div.cell {
                position: relative;
                width: 100%;
                height: 100%;
                line-height: 0px;   /* for IE */

                > div.resize {
                    position: absolute;
                    border: 0px;
                    &.touch { border: 1px; }

                    &.bottom {
                        height: 4px;
                        bottom: -6px;   /* dependent from <td> padding and height */
                        left: -4px;   /* dependent from <td> padding */
                        right: -5px;   /* dependent from <td> padding */
                        &.touch { height: 12px; }
                    }

                    &.right {
                        width: 4px;
                        right: -6px;   /* dependent from <td> padding and width */
                        top: -4px;   /* dependent from <td> padding */
                        bottom: -5px;   /* dependent from <td> padding */
                        &.touch { width: 12px; }
                    }
                }

                > div.cellcontent:focus {
                    outline-color: transparent;
                }
            }
        }

        &.size-exceeded td {
            padding: 0;

            > .placeholder {
                position: relative;
                height: 90px;
                .background-icon {
                    line-height: 88px;
                    i { font-size: 72px; }
                }
            }
        }
    }

    /* change tracking in tables */
    > div.pagecontent > table {
        tr, td {
            &[data-change-track-inserted=true] { background-color: @ct-table-insert-color !important; }
            &[data-change-track-removed=true] { background-color: @ct-table-remove-color !important; }
        }
    }

    /* default table border to prevent shaking */
    & table {
        tr, td {
            border: 1px dotted transparent;
        }
    }

    /* show helper lines when selection is inside table, or when hovering the table */
    &.edit-mode table {
        &.selected {
            .drop-shadow(3px);
        }
        &.selected td, &:hover td {
            border: 1px dotted fade(black, 25%);
        }
    }

    /* floated elements (drawings and offset helper divs) */
    .float {
        display: block;

        &.left { float: left; clear: left; }
        &.right { float: right; clear: right; }
    }

    /* fade out remaining document while highlighting quick-search matches */
    &.highlight > .pagecontent {
        opacity: 0.7;
    }

    .collaborative-overlay {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        z-index: 42;

        div, svg {
            position: absolute;
            pointer-events: none;
        }

        .collaborative-cursor {
            border-left: 2px solid black;
            height: 1.3em;
        }

        .collaborative-cursor-handle {
            pointer-events: auto;
            left: -2px;
            height: 1.5em;
            width: 4px;
        }

        .collaborative-username {
            top: 1.5em;
            left: -2px;
            min-width: 70px;
            padding: 2px 7px;
            .drop-shadow(3px);
            line-height: normal;
            .light-heading-font(@standard-font-size);
            color: white;
            white-space: nowrap;
            display: none;
        }

        .collaborative-selection-group {
            .selection-overlay {
                .opacity(0.3);
            }
            .selection-overlay.drawing {
                position: relative;
                width: 100%;
                height: 100%;
                borderWidth: 2px;
                border-style: solid;
                opacity: 1;
                background-color: transparent;
            }
        }

        .define-scheme-color-rules(collab-color-styles; @i; @color) {
            .user-@{i} {
                background-color: @color;
                border-color: @color;
            }
        }
        .generate-scheme-color-rules(collab-color-styles);

        .drop-caret {
            position: absolute;
            top: 0;
            left: 0;
            height: 1.5em;
            width: 0;
            border-right: 3px solid #3774a8;
            transition: top 0.2s ease-out, left 0.2s ease-out;
            display: none;
        }
    }

    .collaborative-overlay.target-overlay {
        z-index: 41;
    }

    .header, .footer {
        text-align: left;
        line-height: 14px;
        position: relative;
    }

    .flexfooter {
        .display-flex();
        .flexcontent {
            width: 100%;
            .align-self(flex-end);
        }
    }

    .header .cover-overlay, .footer .cover-overlay {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 99;
    }

    .header .textdrawinglayer {
        overflow: visible;
        clip: rect(0, auto, 2000px, 0);
    }

    .footer .textdrawinglayer {
        overflow: visible;
        clip: rect(-2000px, auto, auto, 0);
    }

    .page-break {
        position: relative;
        clear: both;
        .user-select-none-without-moz();
        background: #fff;
        z-index: 40; /* remote selection needs to go bellow page break*/

        .inner-pb-line {
            background: #f0f0f0;
            height: 6px;
            border-bottom: 1px solid #e1e1e1;
            border-top: 1px solid #e1e1e1;
            .user-select-none-without-moz();
            cursor: default;
        }
    }

    .pb-row td {
        padding-bottom: 1px!important; /* fix table row visibility after page break */
    }

    .no-margin-bottom {
        margin-bottom: 0!important;
    }

    .inactive-selection {
        .opacity(0.5);
        .user-select-none-without-moz();
        cursor: default;
        .drawing {
            pointer-events: none!important;
        }
    }

    .inactive-selection::-moz-selection, .inactive-selection .p::-moz-selection, .inactive-selection .p > span::-moz-selection {
        background:#fff;
    }

    .inactive-selection::selection, .inactive-selection .p::selection, .inactive-selection .p > span::selection {
        background:#fff;
    }

    .inactive-selection div.p.selectionHighlighting {
        background: none!important;
    }

    .inactive-selection span, .inactive-selection table, .inactive-selection div{
        .user-select-none-without-moz();
    }

    .active-selection {
        .opacity(1);
        .user-select(text);
        cursor: auto;
        background-color: white;
    }

    .header-wrapper, .footer-wrapper {
        position: relative;
        background: #fff;
    }

    .header.active-selection {
        border-bottom: 1px dashed black;
        outline: none;
    }

    .footer.active-selection {
        border-top: 1px dashed black;
        outline: none;
    }

    .header-footer-placeholder {
        position: absolute;
        top: -100%;
        bottom: 100%;
        left: 0;
        right: 0;
        overflow: hidden;
    }

    table.selected.tb-split-nb {
        box-shadow : none;
    }

     /* header/footer marginal context menu */
    .marginal-context-menu {
        position: absolute;
        left: 10px;
        z-index: 900; //high value that they are always in front of drawings
        background: white;

        .marginal-menu-type, .marginal-menu-goto, .marginal-menu-close {
            display: inline-block;
            background: #fff;
            padding: 5px 7px;
            border: 1px solid #C0C0C0;
            height: 25px;
            line-height: normal;
            font-size: 12px;
            cursor: pointer;
            .fa {
                margin-left: 5px;
                color: #A9A9A9;
            }

            &:hover {
                background-color: #ececec;
                z-index: 900; //high value that they are always in front of drawings
                .fa-times, .fa-caret-down {
                    color: #000;
                }
            }
        }

        .active-highlight {
            background-color: #8EB3D7;
            &:hover {
                background-color: #7BA6D1!important;
            }
        }

        .marginal-menu-type.active-dropdown {
            background-color: #ececec;
            &:hover {
                background-color: #DFDFDF;
            }
        }
        .marginal-container {
            text-align: left;
            position: absolute;
            background: white;
            border: 1px solid #C0C0C0;
            box-shadow: 2px 2px 3px #dadada;
            left: 0;
            cursor: default;
            > div {
                cursor: pointer;
                padding: 7px 15px;
                white-space: nowrap;
                &:hover {
                    background-color: #F3F3F3;
                    &>.marginal-menu-type.active-dropdown {
                        background-color: #ececec;
                    }
                }
            }
            .marginal-separator-line {
                height: 1px;
                margin: 5px 0;
                background-color: #e0e0e0;
                padding: 0;
                &:hover {
                    cursor: default;
                    background-color: #e0e0e0;
                }
            }
        }
    }

    .marginal-context-menu.below-node {
        bottom: -24px;
        padding-top: 1px;
        .marginal-menu-type, .marginal-menu-goto, .marginal-menu-close {
            margin-right: -1px;
        }
        .marginal-container {
            top: 24px;
        }
    }

    .marginal-context-menu.above-node {
        top: -24px;
        padding-bottom: 1px;
        .marginal-menu-type, .marginal-menu-goto, .marginal-menu-close {
            margin-right: -1px;
        }
        .marginal-container {
            bottom: 25px;
        }
    }

    /* end of header/footer marginal context menu */

    .field-tooltip-box {
        padding: 10px 8px;
        color: #000;
        position: absolute;
        left: 0;
        top: 100%;
        margin-top: 10px;
        white-space: nowrap;
        z-index: 2;
        border-radius: 5px;
        border: 1px solid #000;
        background: #ffd;
        box-shadow: 2px 2px 2px #aaa;
    }

    .cx-field-highlight {
        background-color: #aaa;
        opacity: 0.2;
        z-index: 41; /* 1 higher than page break, for highlighting field inside headers*/
    }

    .empty-field {
        position: absolute!important;
        width: 5px;
        margin-left: -1px;
        height: 100%;
        opacity: 0.5;
        &:hover {
            background: #bbb!important;
        }
    }
    .hiddenVisibility {
        visibility: hidden;
    }

} /* end of .page */

/* Special CSS for Safari/Safari mobile to enable cursor traveling between tabs.
    Note this fix doesn't solve the problem that on Safari mobile (iOS) you
    cannot traveling within tabs chains. The browser jumps over the complete
    chain but doesn't stuck anymore.
*/
> .app-pane.browser-safari {

    /* special formatting for empty spans in document */
    .page div.p > span:not(:first-child):empty {
        display: inline-block;
    }

    /* bug 25961: (iPad) enable user to select the last position of a tab chain at the end of the document. */
    &.touch .page div.p:last-child > span:last-child:empty:after {
        content: '\200B';
    }
}

div.resizeline {
    position: absolute;
    border: 1px dotted black;
}

/* Special class for small devices or small browsers */

.app-content-root.small-device {

    .app-content {
        margin: 15px !important;
    }
}
/* Special class for devices < 7" entering draft mode */
.app-content-root.draft-mode {
    .app-content {
        width: 100% !important;
        margin: 0 !important;

        div.page {
            padding: 2% !important;
            width: 96% !important;

            div.pagecontent {
                max-width: 100%;
                position: relative;
                overflow: hidden;
            }

            table {
                left: 0px;
                top: 0px;
                -webkit-transition: all 0s ease-in-out;
                -moz-transition: all 0s ease-in-out;
                transition: all 0s ease-in-out;
                position: relative;
                max-width: unset; /* ff only - to reset max width of table */
            }

            div.drawing {

                div.content:not(.textframecontent) {
                    max-width: 100%;
                    height: auto !important;
                    position: relative;

                    .cropping-frame {
                        position: relative;

                        img {
                            max-width: 100%;
                            height: auto !important;
                        }
                    }
                }
            }
        }
    }
}

/* debugging --------------------------------------------------------------- */

/* debug highlighting for document contents */
&.debug-highlight .page {

    /* paragraph nodes */
    div.p {
        .debug-highlight-box(#8f8);

        /* text portions in paragraph */
        > span {
            .debug-highlight-box(#88f);
        }

        /* special text components in paragraph counting as one character */
        > div.inline:not(.drawing) {
            .debug-highlight-box(#f8f);
        }

        /* helper nodes in paragraph */
        > div.list-label {
            .debug-highlight-box(#f88);
        }
    }

    /* other document content nodes */
    table, div.drawing {
        .debug-highlight-box(#f88);
    }
}

/* ========================================================================= */

} /* end of .io-ox-office-text-main { */

/* GUI additions for drop-down menu elements =============================== */

/* TODO: drop down menus do not have a specific application selector (.io-ox-office-text-main) */
.io-ox-office-main.popup-container.popup-menu {

    /* ListStylePicker control --------------------------------------------- */

    &.list-style-picker .section-container .button {
        text-align: center;
        > .caption { min-width: 28px; }
    }

    /* ParagraphStylePicker control ---------------------------------------- */

    &.app-text.style-picker.family-paragraph .section-container .button {
        width: 128px;
    }

    &.change-track-popup {

        .change-track-badge {

            display: table !important;
            padding: 5px 10px;
            width: 100%;

            .photo-container {
                display: table-cell;
            }

            .change-track-description {
                display: table-cell;
                font-weight: bold;
                line-height: 1.5em;
                vertical-align: middle;
                text-align: left;
                padding-left: 10px;

                .change-track-author {
                    display: block;
                    width: 160px;
                    padding-right: 5px;
                    color: #f89406;
                    .ellipsis();

                    .define-scheme-color-rules(ct-description-color; @i; @color) {
                        &[data-user-index="@{i}"] { color: @color !important; }
                    }
                    .generate-scheme-color-rules(ct-description-color);
                }

                .change-track-author:hover {
                    cursor: text;
                    text-decoration: none;
                }

                .halo-link:hover {
                    cursor: pointer;
                    text-decoration: underline;
                }
            }

            .change-track-date {
                display: table-cell;
                /* WCAG 2.0 Success Criteria 1.4.3 - Text content must exceed Color Contrast Ratio (CCR) of 4.5 */
                color: #777777;
                font-size: 11px;
                text-align: right;
                vertical-align: top;
            }
        }

        .button { padding:0px 20px !important; }

        /* workaround for android-touches */
        .button > * { pointer-events: none; }

    }

    &.field-format-popup {
        .view-component {
            min-width: 220px;

            .dropdown-group {
                flex-direction: column;
            }

            .field-format-description {
                font-weight: bold;
                padding: 5px 13px;
                text-align: left;
                cursor: default;
            }
            .today-datepicker-wrapper {
                .display-flex();
                margin: 5px 13px 0 13px;
                padding-bottom: 8px;

                a {
                    text-align: center;
                    width: 100%;
                }

                .set-today-btn {
                    border: 1px solid #ccc;
                    border-radius: 3px;
                    display: inline-block!important;
                    flex: 1;
                }

                .toggle-datepicker-btn {
                    display: inline-block!important;
                    border: 1px solid #ccc;
                    border-radius: 3px;
                    margin-left: 13px;
                    width: 50px;
                }

                .disable {
                    cursor: default!important;
                    pointer-events: none!important;
                    a{
                        color: #ccc!important;
                    }
                }
            }
        }
    }

} /* end of .io-ox-office-main.popup-container.popup-menu.app-text { */

/* temporary helper node where paragraphs processed for splitting with page breaks are cloned to  */
#io-ox-office-temp {
    .p {
        word-wrap: break-word;

        .inline.tab, .helper.list-label {
            display: inline-block;
        }

        .drawing.inline {
            display: inline-block;
            vertical-align: bottom;
        }

        /* floated elements (drawings and offset helper divs) */
        .float {
            display: block;

            &.left { float: left; clear: left; }
            &.right { float: right; clear: right; }
        }
    }
}
/* end of #io-ox-office-temp { */

/* special android/IOS code with cursor simulation and contenteditable overwriting */
html.android, html.ios {
    .io-ox-office-main.io-ox-office-text-main > .app-pane {
        .page {
            -webkit-user-modify: read-only;

            /* blinking android cursor simulation */
            .androidblink span.androidcursor {
                border-right: 1px solid black;
                margin-right: -1px;
            }

            .androidblink div.androidcursor {
                border-right: 1px solid black;
                padding-right: -1px;
            }

            span.androidime {
                border-bottom: 1px solid black;
                margin-bottom: -1px;
            }

            /* inherit contenteditable behavior for android selection */
            *[contenteditable=true] {
                -webkit-user-modify: read-only;
            }
        }
        .page.edit-mode {
            -webkit-user-modify: read-write-plaintext-only;

            *[contenteditable=true] {
                -webkit-user-modify: inherit;
            }
        }

        textarea.transparent-selection {
            color: transparent;
            background: transparent;
            border-color: transparent;

            &::selection {
                background: transparent;
            }
        }
    }
}

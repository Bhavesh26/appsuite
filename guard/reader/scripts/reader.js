﻿/// <reference path="jquery-1.7.1.js" />
/* All content on this website (including text, images, source
 * code and any other original works), unless otherwise noted,
 * is licensed under a Creative Commons License.
 *
 * http://creativecommons.org/licenses/by-nc-sa/2.5/
 *
 * Copyright (C) 2016-2020 OX Software GmbH
 */

var userid;
var username;
var cid;
var sessionID;
var auth;
var itemid;
var templid;
var logindata;
var settings;
var attachlist = [];
var uploadlist = [];
var savedata;
var noRecovery = false;


try {// Load root from config file.  Error if missing or unable to determine api address
    var root = getRoot();
} catch (e) {
    alert('missing or corrupt config.js file');
}

var language = getURLParameter('lang').replace(/\./g,'');
templid = parseInt(getURLParameter("templid"));
if (templid == null || isNaN(parseInt(templid))) {   // If no clean template ID, see if we have a default from config.js
    try {
        if (typeof(getDefaultTemplateId) == 'function') {
            templid = getDefaultTemplateId ();
            if (templid == 0) templid = null;
        }
    } catch (e)
    {
        log('missing default ID from config file');
    }
}
if (templid !== null) {
    $('head').append('<link rel="stylesheet" type="text/css" href="./templates/' + templid + '-style.css">');
}

$().ready(function () {// Once ready, load templates.

    // Add addditional templates here.  Specify template id, the template file, and the location
    log ('template id is ', templid);
    // Template for css.  Format cid-style.css
    if (templid !== null) {
        if (!isNaN(parseInt(templid))) {// Check cid is actually int, and if so, load templates styles
            getTemplate(templid, "header.html", "top-bar-placeholder");
            getTemplate(templid, "footer.html", "footer-placeholder");
        }
    }
    $('#top-bar-placeholder').show();
    $('#footer-placeholder').show();

    itemid = getURLParameter("email");
    var option= {
            resGetPath: 'l10n/__ns__-__lng__.json',
            debug: debug,
            detectLngQS: 'lang',
            fallbackLng: 'en_US',
            customLoad: loadTranslations

    };
    i18n.init(option, function(t) {
        $('.view').i18n();
        if (root !== undefined) view('login');
        if (itemid === null) {
            $('#loginHeader').text(i18n.t('Please provide your credentials'));
        }
    })
    .done(function () {
        $.get('./l10n/languages', function (languages) {
            var selectedLanguage = getURLParameter('lang');
            if (selectedLanguage === null) {
                selectedLanguage = "en_US";
            }
            $.each(languages, function (c, n) {
                if (c === selectedLanguage) {
                    $('#languageSelect').append('<li class="active"><a href="#" language="' + c + '">' + n + '</a></li>');
                    $('#languageSelectButton').html(n + ' <span class="caret"></span>');
                } else {
                    $('#languageSelect').append('<li><a href="#" language="' + c + '">' + n + '</a></li>');
                }
            });

            var user = getURLParameter('user');
            if (user !== null) {
                $('#Username').val(user);
                $('#Username').hide();
                $('#loginHeader').text(i18n.t('Please provide your encryption password'));
                $('#Password').focus();
            }
            var newGuest = templid = getURLParameter('newGuest');
            if (newGuest) {
                login();
            }
        }, 'json');

        // Language selection listener
        $("#languageSelect").click(loadLanguage);
        var resetid = getURLParameter('resetid');
        if (resetid != null) {
            view('DoResetDiv');
        }

    });
});

function loadLanguage (ev) {
    var url = document.URL;
    if (url.indexOf('&lang=') > 0) {
        url = url.substring (0, url.indexOf('&lang='));
    }
    url = url + (url.indexOf('?') > 0 ? '': '?') + '&lang=' + $(ev.target).attr('language');
    window.location.replace(url);
}

// Load translation for language.  Custom overwrites.
function fetchTranslations (callback, options) {
    var requests = [];
    var custom, translation = {};
    $.get("l10n/translation-" + language + ".json")
        .done(function(data) {
                $.get("l10n/custom-" + language + ".json")
                .done(function(custdata) {
                    $.extend(data, custdata);
                })
                .always(function () {
                    callback(data);
                });
        })
        .fail(function () {
            console.log('Failed to load language ' + language);
            $.get("l10n/translation-" + options.fallbackLng + ".json")
                .done(function (data) {
                    callback(data);
                })
                .fail(function (e) {
                    console.log(e);
                    alert('Failed to default language');
                });
        });
}

function loadTranslations (lng, ns, options, loadComplete) {
    fetchTranslations (function (data) {
        loadComplete(null, data);
    }, options);
}

// Load the template.  Format should be cid-template .  Location in template subdirectory
function getTemplate (templid, template, locationID) {
    $.get('./templates/' + (templid === null ? "" : (templid + "-")) + template, function (data) {
        $('#' + locationID).replaceWith(data);
    })
    .fail(function () {// If not found with cid, try loading default
        $.get('./templates/' + template, function (data) {
            $('#' + locationID).replaceWith(data);
        });
    });
}


// Provides login to oxguard backend.  Authentication key is returned and stored
function login() {
    username = $('#Username').val();
    var password = $('#Password').val();
    if(typeof String.prototype.trim === 'function') {// Trim not supported in IE
        password = $('#Password').val().trim();
    }
    $('#authorizing').show();
    logindata = {
            username : username,
            password : password
    }
    var url = 'oxguard/login?action=guest';
    // If this is a getkey login, then we need a different auth
    var action = getURLParameter('action');
    if (action === 'getkey') {
        logindata.salt = getURLParameter('token');
        logindata.userid = getURLParameter('userid');
        logindata.cid = getURLParameter('cid');
        url = 'oxguard/retrieve?action=login';
    }
    $.ajax({
        url: root + url,
        type:'POST',
        data: JSON.stringify(logindata),
        contentType:'application/json; charset=utf-8',
        success: function (data) {
            log('loging data: ', data);
            if (data === null) {
                alert('error');
                return;
            }
            if (data.productName) {
                $('head').append('<title>' + data.productName + '</title>');
            }
            if (data.auth === 'Password Needed') {
                $('#login').hide();
                $('#NewPassword').show();
                userid = data.userid;
                cid = data.cid;
                sessionID = data.sessionID;
                auth = data.auth;
                return;
            }
            if (data.auth === 'Bad Password' || data.auth === 'No Key') {
                if (password === '') {
                    $('#authorizing').hide();
                    return;  // This was a blank login attempt
                }
                //#. Bad username or Password at login
                $('#ErrorBox').html(i18n.t('Bad username / Password'));
                $('#ErrorBox').show();
                $('#Username').show();
                $('#Password').val('').focus();
                $('#authorizing').hide();
                /*
                if (data.recoveryAvail === false) {
                    $('.forgot').hide();
                } else {
                    $('.forgot').show();
                }
                */
                return;
            }

            if (data.auth === 'lockout') {
                $('#ErrorBox').html(i18n.t('Account locked out try later'));
                $('#ErrorBox').show();
                $('#Username').hide();
                $('#Password').hide();
                $('#LoginButton').hide();
                $('#authorizing').hide();
                /*
                if (data.recoveryAvail === false) {
                    $('.forgot').hide();
                }
                */
                return;
            }
            if (data.auth === 'not found') {
                $('#ErrorBox').html(i18n.t('Account not found'));
                $('#ErrorBox').show();
                $('#authorizing').hide();
                return;
            }
            if (data.auth === 'new') {
                view('newUser');
                $('#welcome').text(i18n.t('Welcome to $s').replace('$s', data.productName));
                if (data.pin) {
                    $('#pinDiv').show();
                }
                settings = data.settings;
                if (data.settings && data.settings.noRecovery) {
                    $('#newQA').hide();
                }
                return;
            }
            noRecovery = data.settings.noRecovery;
            if (data.auth.length > 20) {
                okLogin(data, action);
                return;
            }
            if (data.auth === 'PIN') {
                $('#authorizing').hide();
                $('#pin').show();
                $('#pinbox').focus();
                return;
            }
        }

    })
    .fail(function (data) {
        $('#authorizing').hide();
        alert('fail');
    });
}

function okLogin(data, action) {
    userid = data.userid;
    cid = data.cid;
    sessionID = data.sessionID;
    auth = data.auth;
    if (action === 'getkey') {
        rKeys(data);
        return;
    }
    settings = data.settings;
    itemid = getURLParameter("email");
    if (itemid === null) {
        $('.back').remove(); // remove back button if PGPManage only option
        view('pgpManage');
    } else {
        var extrapass = getURLParameter("extrapass");
        if (extrapass !== null) extrapass = extrapass.trim();
        if (extrapass === 'true') {
            getExtraPass(data);
        } else {
            openreader(data, "");
        }
    }
    logindata = null;
}

function loginPin() {
    logindata.pin = $('#pinbox').val();
    $.ajax({
        url: root + "oxguard/login?action=guest",
        type:'POST',
        data: JSON.stringify(logindata),
        contentType:'application/json; charset=utf-8',
        success: function (data) {
            try {
                log("loging data: ", data);
                if (data.auth.length > 20) {
                    okLogin(data);
                    return;
                }
                if (data.auth === "Bad") {
                    $('#PinErrorBox').html(i18n.t("Bad PIN")).show();
                    $('#pinbox').val('').focus();
                    return;
                }
                if (data.auth === "lockout") {
                    $('#PinErrorBox').html(i18n.t("Account locked out try later")).show();
                    $('#pinbox').val('').hide();
                    $('#PinButton').hide();
                    return;
                }
            } catch (e) {
                $('#PinErrorBox').html(i18n.t("Error checking PIN")).show();
                log(e);
            }
        }
    })
    .fail(function () {
        $('#PinErrorBox').html(i18n.t("Error checking PIN")).show();
        return;
    });
}

function firstPassChange() {
    var newpass = $('#firstpass').val();
    var newpass2 = $('#firstpass2').val();
    if (newpass !== newpass2) {
        $('#firstErrorDiv').text(i18n.t('Passwords not equal'));
        $('#firstErrorDiv').show();
        $('#firstpass').focus();
        return;
    }
    var question = $('#newQuestion').val();
    var answer = $('#newAnswer').val();
    if (settings.noRecovery === false) {
        if (answer.length < 2) {
            $('#firstErrorDiv').text(i18n.t('Answer required for password reset option'));
            $('#firstErrorDiv').show();
            $('#newAnswer').focus();
            return;
        }
    }
    var itemId = getURLParameter("email");
    var pin = $('#newpin').val();
    var passdata = {
            newpass: newpass,
            question: question,
            answer: answer,
            pin: pin,
            email: $('#Username').val(),
            itemId: itemId
        };
    showWait();
        $.ajax({
            url: root + 'oxguard/guest?action=firstpass',
            type:'POST',
            data: JSON.stringify(passdata),
            contentType:'application/json; charset=utf-8',
            success: function (data) {
                $('#firstErrorDiv').hide();
                if (data instanceof Array) data = data[0];
                if (data && data.error) {
                    hideWait();
                    if (data.code === 'GRD-CHANGE-PW-0002') {
                        $('#firstErrorDiv').text(i18n.t('PIN invalid or missing'));
                        $('#firstErrorDiv').show();
                        $('#newpin').focus();
                        return;
                    }
                    if (data.code === 'GRD-CHANGE-PW-0001') {
                        $('#firstErrorDiv').text(i18n.t('Password Length Too Short'));
                        $('#firstErrorDiv').show();
                        $('#firstpass').focus();
                        return;
                    }
                    alert (data.error + ' ' + data.error_desc);
                    return;
                }
                if (data.auth && data.auth.length > 20) {
                    auth = data.auth;
                    okLogin(data);
                }
            }
        })
        .fail(function (data) {
            $('#firstErrorDiv').text(i18n.t('Problem connecting with the server'));
            $('#firstErrorDiv').show();
            alert(data);
        })
        .always(function () {
            hideWait();
        });

}

function showChangepass() {
    view('NewPassword');
}

function showWait() {
    $('#waitDiv').show();
}

function hideWait() {
    $('#waitDiv').hide();
}

function comparePass(pass1, pass2, err1, err2) {
    var val1 = $('#' + pass1).val();
    var val2 = $('#' + pass2).val();
    if ((val1 != val2) && (val2 != '')) {
        $('#' + err2).text(i18n.t('Passwords do not match'));
    } else {
        $('#' + err2).text('');
    }
    if (settings.min_password_length && (val1.length < settings.min_password_length)) {
        $('#' + err1).text(i18n.t('Too short'));
    } else {
        $('#' + err1).text('');
    }
}

// Change the oxguard password
function changePass() {
    var pass1 = $('#new1').val();
    if (pass1 !== $('#new2').val()) {
        $('#new1').focus();
        $('#ChangeError').html(i18n.t("Passwords do not match"));
    $('#ChangeError').show();
        return;
    }
    var userdata = {
        newpass: pass1,
        oldpass: $('#old').val(),
        user_id: userid,
        sessionID: sessionID,
        cid: cid
    };
    $.ajax({
        url: root + 'oxguard/guest?action=changepass&session=' + sessionID + '&auth=' + encodeURIComponent(auth),
        type:'POST',
        data: JSON.stringify(userdata),
        contentType:'application/json; charset=utf-8',
        success: function (data) {
            if (typeof data === 'string') data = $.parseJSON(data);
            log('success ', data);
            if (data.auth && data.auth.length > 20) {
                auth = data.auth;
                $('#new1').val('');
                $('#new2').val('');
                $('#old').val('');
                $('#ChangeError').removeClass('alert-danger').addClass('alert-success');
                $('#ChangeError').html(i18n.t("Password Changed"));
                $('#ChangeError').show();
                window.setTimeout (function() {
                    $('#ChangeError').html();
                        $('#ChangeError').hide();
                        view("Reader");
                }, 2000);
            }
            if (data instanceof Array) data = data[0];
            if (data && data.error) {
                $('#ChangeError').html(i18n.t("Failed to change password" + ': ') + data.error);
                $('#ChangeError').removeClass('alert-success').addClass('alert-danger').show();
            }
        }

    })
    .fail(function () {
        $('#ChangeError').html(i18n.t("Failed to change password"));
        $('#ChangeError').removeClass('alert-success').addClass('alert-danger').show();
    });
}

function showChangePassError (text) {
    $('#firstChangeError').html(text);
    $('#firstChangeError').show();
    if ($(window.top).height() < 800) // If small, scroll to bottom
        $('.passchange').animate ({ scrollTop: $('#firstChangeError').offset().top }, 1000);
}

//Change the oxguard password
function firstChangePass() {
    var pass1 = $('#change1').val();
    if (pass1.length < 8) {
        $('#change1').focus();
        showChangePassError(i18n.t("Password must be at least 8 characters long"));
        return;
    }
    if (pass1 !== $('#change2').val()) {
        $('#change1').focus();
        showChangePassError(i18n.t("Passwords do not match"));
        return;
    }
    var answer = $('#answer').val();
    if (answer.length < 3 && !noRecovery) {
        $('#answer').focus();
        showChangePassError(i18n.t("Please provide an answer for the security question"));
        return;
    }
    var password = $('#Password').val();
    if(typeof String.prototype.trim === 'function') { // Trim not supported in IE
        password = password.trim();
    }
    var userdata = {
        newpass: pass1,
        oldpass: password,
        question: $('#question').val(),
        answer: $('#answer').val(),
        user_id: userid,
        sessionID: sessionID,
        cid: cid
    };
    log("changepass data: ", userdata);
    $.ajax({
        url: root + 'oxguard/guest?action=changepass&session=' + sessionID + '&auth=' + encodeURIComponent(auth),
        type:'POST',
        data: JSON.stringify(userdata),
        contentType:'application/json; charset=utf-8',
        success: function (data) {
            if (typeof data === 'string') data = $.parseJSON(data);
            log('success ', data);
            if (data.auth.length > 20) {
                auth = data.auth;
                sessionID = data.sessionID;
                $('#new1').val('');
                $('#new2').val('');
                $('#firstChangeError').html(i18n.t("Password Changed"));
                $('#firstChangeError').removeClass('alert-danger').addClass('alert-success').show();
                window.setTimeout(function () {
                    $('#firstChangeError').html();
                    $('#firstChangeError').hide;
                    $('#firstChangePassDiv').hide();
                },1000);
                $('#Reader').css("position", "inherit");
            }
        }
    })
    .fail(function () {
        showChangePassError(i18n.t("Failed to change password"));
        $('#firstChangeError').removeClass('alert-success').addClass('alert-danger').show();
    });
}


// Extra password is required
function getExtraPass (data) {
    view("ExtraPassDiv");
    $('#ExtraPassButton').click(function () {
        var extra = $('#extrapass').val();
        openreader(data, extra);
    });
    $('#extrapass').focus()
    .keypress(function (e) {
        if (e.which == 13) {
            $('#ExtraPassButton').click();
        }
    });
}

// Open the email for reading.  Populate with decoded data
function openreader(ldata, extra) {
    var lastMod = ldata.lastMod;
    view();
    $('#decoding').show();
    itemid = getURLParameter("email");
    var json = {
            cid : cid,
            item : itemid,
            extrapass : (extra === undefined) ? '' : extra
    }
    var url = root + 'oxguard/guest?action=getmail&session=' + sessionID + '&auth=' + encodeURIComponent(auth);
    $.ajax({
        url: url,
        type:'POST',
        data: JSON.stringify(json),
        contentType:'application/json; charset=utf-8',
        success: function (data) {
            $('#Reader').show();
            if (typeof data === "string") {
                data = JSON.parse(data);
            }
            log("reader data: ", data);
            if (data === null) {
                fail();
                return;
            }
            var toList = getTo(data),
                ccList = getCC(data);
            $('#sent').html('<td class="tdname">' + i18n.t("Sent") + '</td><td><span class=\"oxlabelcontent\">' + prepHTML(getSent(data)) + '</span></td>');
            if (toList.length > 2) {
                $('#tolist').html('<td class="tdname">' + i18n.t("To") + '</td><td><span class=\"oxlabelcontent\">' + prepHTML(toList) + '</span></td>').show();
            }
            log("cc: ", ccList);
            if (ccList.length > 2) {
                $('#cclist').html('<td class="tdname">' + i18n.t("Cc") +'</td><td><span class=\"oxlabelcontent\">' + prepHTML(ccList) + '</span></td>').show();
            }
            var fromTd = $('<td>');

            fromTd.append(getSignature(data));
            fromTd.append('<span class=\"oxlabelcontent\">' + prepHTML(getFrom(data)) + '</span>');
            $('#fromlist').html('<td class="tdname">' + i18n.t("From") +'</td>').append(fromTd);
            $('#subject').text(getSubject(data));
            $('#attdiv').remove();
            var readercont = $('#reader').contents().find('html');
            $('#reader').contents().find('html').html(replace_content_id(getContent(data), data, itemid));
            $('#reader').contents().find('blockquote').css({'border-left': '1px solid #08C', 'padding': '7px 0 7px 19px', 'margin': '1em 0', 'opacity': '.5'});
            $('#header').after(getAttachments(data, itemid, auth));
            $('#decoding').hide();
            $('#reply').unbind().click(function() {
                reply(data, itemid);
            });
            $('#replyall').unbind().click(function() {
                replyall(data, itemid);
            });
            $('#reply-bottom').unbind().click(function() {
                    reply(data, itemid);
            });
            $('#replyall-bottom').unbind().click(function() {
                    replyall(data, itemid);
            });
            // Some squeeze for mobile
            if (window.screen.height < 800) {
                log('mobile detected');
                $('.padding').hide();
                $('.attachments').css('padding', '0');
            }
            log("last mod: ", lastMod);
            if (lastMod === 'null') {
                log('for changepass');
                window.setTimeout(function () {
                    $('#Reader').css("position", "fixed");
                    if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
                        $('.passchange').css('height', '100%').css('width', '100%').css('margin-top', '0%')
                        .bind('touchstart', function (e) {
                            if (e.target.type !== 'password' && e.target.type !== 'text') {
                                document.activeElement.blur();
                            }
                        });
                    }
                    $('#firstChangePassDiv').show();
                    if (noRecovery) {
                        $('.qaDiv').hide();
                        $('.passchange').css('height', '50%').css('min-height', '400px');
                    }
                },2000);
            }
            // Make all name columns same size.  Check for largest
            window.setTimeout(function() {
                var max = 0;
                $('.tdname').each(function(v, e) {
                    var w = $(e).width();
                    if (w > max) max = w;
                });
                $('.tdname').width(max + 2);
            }, 100);
        }

    })
    .fail(function (data) {
        log('fail', data);
        var resp = data.responseText;
        switch (resp.trim()) {
        case "badextra":
            fail("Bad Password");
            break;
        case "not found":
            view("NotFound");
            savedata = ldata;
            break;
        case "retracted":
            fail(i18n.t("This email is no longer available"));
            break;
        case "expired":
            fail(i18n.t("This email is no longer available"));
            break;
        default:
            fail();
            break;
        }

    });

}

function getSignature (data) {
    var signedicon = '';
    if (data.results) {
        if (data.results.signature === true) {
            if (data.results.verified === true) {
                // valid signature
                signedicon = $('<i class="oxguard_icon_fa fa-pencil-square-o fa oxguard_signed" aria-hidden="true" title="' + i18n.t('This Email was signed and verified') + '"/>');
            } else {
                // invalid signature
                $('<i class="oxguard_icon_fa_alert fa-pencil-square fa oxguard_signed" aria-hidden="true" title="' + i18n.t('This Email was signed and failed verification') + '"/>');
            }
        }
    }
    return (signedicon);
}

function fail(alerttype)
{
    if (alerttype === undefined) {
        alert(i18n.t("An error occurred"));
    } else {
        alert(alerttype);
    }
    $('#authorizing').hide();
    $('#decoding').hide();
    view('login');
    log('fail');
}

function replace_content_id (content, data, item) {
    content = content.replace(/cid:[^@]+@GUARD/g, function (c) {
        var params = '&email=' + item +
        '&session=' + sessionID +
        "&userid=" + userid +
        "&cid=" + cid +
        '&auth=' + encodeURIComponent(auth) +
        '&lang=' + language +
        '&content_id=' + encodeURIComponent(c.substring(4));
        var link = getFullRoot() + 'oxguard/guest?action=getattachcid' + params;

        return (link);
    });
    return (content);
}

function getFullRoot () {
    // If root already has domain, just return
    if (root.indexOf('http') > -1) {
        return root;
    }
    return 'https://' + document.domain + '/' + root;
}

// Get base url
function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}

function cancelprop(e) {
    e.cancelBubble = true;
    e.returnValue = false;
    // non-ie
    e.preventDefault && e.preventDefault();
    e.stopPropagation && e.stopPropagation();
}

// Gets the language for TinyMCE from lang parameter in url
function getTinyLanguage () {
    var lang = getURLParameter('lang');
    var tinylang = "en";
    if (lang !== null) {
        $.get('./scripts/tinymce/langs/' + lang.toLowerCase() + '.js', function (data) {
            return(lang);
        })
        .fail (function () {
            lang = lang.toLowerCase();
            $.get('./scripts/tinymce/langs/' + lang.substring(0,2) + '.js', function (data) {
            log('setting tinylanguage to ' + lang.substring(0,2));
            return(lang.substring(0,2));
        });
        });
        return (tinylang);// otherwise return default
    }
}

// Add surrounding <> for emails
function surround(d) {
    return (' <' + d + '>');
}

function getList(recips) {
    var list = '';
    for (var i = 0; i < recips.length; i++) {
        if (i > 0) list += ', ';
        list += recips[i][0] + surround(recips[i][1]);
    }
    log("to: ", list);
    return (list);
}

// Get the to recipients
function getTo(data) {
    var recips = data.to;
    return (getList(recips));
}

function getReplyAllTo(replyto) {
    return (getList(replyto.replyAll))
}

function getCC(data) {
    var recips = data.cc;
    var list = '';
    if (recips === undefined) return (list);
    for (var i = 0; i < recips.length; i++) {
        if (i > 0) list += ', ';
        list += recips[i][0] + surround(recips[i][1]);
    }
    log("cc: ", list);
    return (list);
}

function getFrom(data) {
    try {
        return (data.from[0][0] + surround(data.from[0][1]));
    } catch (e) {
        console.log(e);
        return ('');
    }

}

function getSubject(data) {
    return (data.subject);
}

function prepHTML (data) {
    if (data === undefined) return ('');
    return $('<div/>').text(data).html();
}

function getSent(data) {
    var sent = new Date(Date.parse(data.sent));
    return (sent.toLocaleDateString() + " " + sent.toLocaleTimeString());
   // return(data.sent);
}

function getContent(data) {
    var attachments = data.attachments;
    var othertext = '';
    for (var i = 0; i < attachments.length; i++) {
        if (attachments[i].disp === 'inline') {
            if (attachments[i].content_type === 'text/html') {
                return (attachments[i].content);
            } else {
                othertext = attachments[i].content;
            }
        }
    }
    othertext = othertext.replace(/\n/g, '<br/>');
    return (othertext);
}

// Generate attachments.
function getAttachments(data, item, auth) {
    var attachmentdiv = $('<div id="attdiv"/>');
    var atttable = $('<table>');
    var row1 = $('<tr>');
    var attachments = data.attachments;
    var attcount = 0;
    var attd = $('<td>');
    for (var z = 0; z < attachments.length; z++) {
        var attach = attachments[z];
        if (attach.disp === 'attachment') {//If it is an attachment, we are going to display
            if (attcount === 0) {
                row1.append('<td class="io-ox-label" id="attlabel" style="vertical-align:top; padding-top:5px;">' + i18n.t("Attachment") + '</td>');
            }
            attcount++;
            // Create the link for that attachment
            var params = '&email=' + item +
            '&session=' + sessionID +
            "&userid=" + userid +
            "&cid=" + cid +
            (data.extrapass ? ('&encrextrapass=' + encodeURIComponent(data.extrapass)): "") +
            '&lang=' + language +
            '&attname=' + encodeURIComponent(attach.filename);
            var name = attach.filename.replace('.grd2','').replace('.grd','');
            var link = root + 'oxguard/guest/' + encodeURIComponent(name) + '?action=getattach' + params;
            var atlink = $('<a>', {
                "class": "attachment-link",
                text: name,
                "link": link,
                "href" : "#"
            }).css({
                'padding-right': '7px'
            }).click(function() {
                window.open($(this).attr("link") + "&auth=" + encodeURIComponent(getauth()));
            });
            attd.append($('<div class="attachment-link">').append(atlink));
        }
    }
    row1.append(attd);
    // Resize
    window.setTimeout(function () {
        try {
            var size = window.screen.width - $('#attlabel').width() - 50;
            $('.attachment-link').css('max-width', size);
        } catch (e) {
            console.log(e);
        }
    }, 100);
    return (attachmentdiv.append(atttable.append(row1)));
}

function getauth () {
    return(auth);
}

function getReplyData (data) {

    var replyTo = JSON.parse(JSON.stringify(data.from));
    var replyAll = JSON.parse(JSON.stringify(data.from));
    var thisEmail = $('#Username').val();
    var from = ["", thisEmail];
    // Loop through recipient list, make sure not this user, and if found, update name
    for (var i = 0; i < data.to.length; i++) {
        if (data.to[i][1] === thisEmail) {
            from = data.to[i];
        } else {
            replyAll.push(data.to[i]);
        }
    }
    if (data.cc !== undefined) {
        for (var j = 0; j < data.cc.length; j++) {
            if (data.cc[j][1] === thisEmail) {
                from = data.cc[j];
            } else {
                replyAll.push(data.cc[j]);
            }
        }
    }

    return {
        replyTo : replyTo,
        replyAll : replyAll,
        from : from
    }
}

function changeAttachments(attachments) {
    // Push to attachment list
    for (var i=0; i<attachments.files.length; i++) {
        attachlist.push(attachments.files[i]);
    }
    $(attachments).val('');  // Clear file list
    drawAttachments();

}




// Draw attachments for reply
function drawAttachments () {
    var list = '';
    $('.deleteAttach').unbind();
    for (var i=0; i< attachlist.length; i++) {
        if (i === 0) {
            list = createLink(attachlist[i]);
        } else {
            list = list + createLink(attachlist[i]);
        }
    }

    $('#attachmentList').html($('<ul>').append(list));
    $('.deleteAttach').click(function (e) {
        deleteAttachment($(e.target).parent().attr('data'));
        drawAttachments ();
    });
}

function createLink (file) {
    if (file === undefined) return('');
    var name = file.name;
    var div = $('<ul>');
    var span = $('<li>');
    var deletelink = $('<a href="#" id="delete-' + name + '" data="' + name + '" class="deleteAttach"></a>');
    var trash = $('<i class="fa fa-trash-o"></i>');
    deletelink.append(trash);
    div.append(span.append(deletelink).append(name));
    return (div.html());
}

function deleteAttachment (name) {
    for (var i =0; i < attachlist.length; i++) {
        if (attachlist[i] !== undefined) {
            if (attachlist[i].name.trim() === name.trim()) {
                delete attachlist[i];
                return;
            }
        }
    }
}

// Send the reply
function sendreply(data, all) {
    var replydata = getReplyData (data);
    log('replydata', replydata);
    var to = replydata.replyTo;
    if (all) {
        to = replydata.replyAll;
    }
    log('reply to ', to);
    var from = replydata.from;
    //#. For the prefix of the subject reply I.E. Re: The message you sent
    var subject = i18n.t("re") + ': ' + data.subject;
    var format = $('[name="format"]:checked').val();
    var content;
    if (format === 'html') {
        content = tinyMCE.activeEditor.getContent();
    }
    else {
        content = $('.plaintexteditor').val();
    }

    var mail = createJson (to, from, subject, content, data, format);
    log (mail);
    var formData = new FormData();
    formData.append('json_0', JSON.stringify(mail));
    log('attachments', attachlist);
    for (var l = 0; l < attachlist.length; l++) {
        if (attachlist[l] !== undefined)
            formData.append('file' + l, attachlist[l]);
    }
    var xhr = new XMLHttpRequest();
    var url = root + 'oxguard/mail?action=emailform' +
    '&session=' + sessionID + '&userid=' + userid + '&cid=' + cid + "&guest=true&sr=" + itemid + "&auth=" + encodeURIComponent(auth) +
    (!isNaN(parseInt(templid)) ? ("&templid=" + templid) : "");
    $.ajax({
        url: url,
        type: "POST",
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,   // tell jQuery not to set contentType
        xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress',function(ev) {
                    if (ev.lengthComputable) {
                        var percentComplete = Math.floor(ev.loaded * 100 / ev.total);
                        $('#sendingProgress').attr('aria-valuenow', percentComplete)
                        .css('width', percentComplete + '%');
                        log('Uploaded '+percentComplete+'%');
                        // update UI to reflect percentUploaded
                    }
               }, false);
            }
            return myXhr;
        },
        success: function(data){
            view("results");
            $('#sendresult').html(i18n.t("Encrypted email reply has been sent successfully"));
            window.setTimeout(function () {
                view("Reader");
            },2000);
            $('#sending').hide();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            log(errorThrown);
            view("results");
            if (XMLHttpRequest.responseText.trim() === 'not authorized') {
                $('#sendresult').html(i18n.t("The authorization to send a reply was not valid  Did you have another reader open?  Please reload and log in again"));
            } else {
                $('#sendresult').html(i18n.t("An error occurred during sending the encrypted email reply"));
            }

            $('#sending').hide();
        }
      });
    $('#sending').show();

}

function reply(data, itemid) {
    initReply(data, false, itemid);
    $('#rto').html('<span>' + prepHTML(getFrom(data)) + '</span>').show();
}

function replyall(data, itemid) {
    initReply (data, true, itemid);
    var replydata = getReplyData (data);
    $('#rto').html('<span>' + prepHTML(getReplyAllTo(replydata)) + '</span>').show();
}

function initReply (data, replyall, itemid) {
    $('[name="format"][value="html"]').prop("checked", true);
    $('.plaintexteditor').val('');
    attachlist = [];
    drawAttachments();
    $('#rsubject').text(i18n.t("re") + ': ' + data.subject);
    var bm = tinyMCE.activeEditor.selection.getBookmark();
    tinyMCE.activeEditor.setContent(replace_content_id(formatReply(data), data, itemid));
    tinyMCE.activeEditor.selection.moveToBookmark(bm);
    tinyMCE.activeEditor.focus();
    view('replydiv');
    $('#SendReply').unbind().click(function() {
        sendreply(data, replyall);
    });
    $('.plaintexteditor').hide();
    $('.mce-tinymce').show();
}

function formatReply (data) {
    var orig = '<span>' + i18n.t("Original message from") + ' ' + getFrom(data) + ' ' + i18n.t("Sent") + ' ' + getSent(data) + '</span><div style="float:none; height:10px;"></div>';
    var message = '<br/><br/><blockquote type="cite">' + orig + getContent(data) + "</blockquote><br/>";
    return (message);
}

function changeformat(radio) {
    if ($(radio).val() === 'html') {
        var text = $('.plaintexteditor').val();
        var bm = tinyMCE.activeEditor.selection.getBookmark();
        tinyMCE.activeEditor.setContent(convertPlain(text));
        tinyMCE.activeEditor.selection.moveToBookmark(bm);
        tinyMCE.activeEditor.focus();
        $('.plaintexteditor').hide();
        $('.mce-tinymce').show();
    } else {
        var content = tinyMCE.activeEditor.getContent();
        var editor = $('.plaintexteditor');
        editor.val(unescapeHtml(convertHTML(content))).show().focus().scrollTop(0);
        editor[0].setSelectionRange(1,1);
        editor[0].focus();
        $('.mce-tinymce').hide();
    }
}

function unescapeHtml(text) {
    return text.replace(/&amp;/g, '&')
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&quot;/g, '"')
        .replace(/&nbsp;/g, ' ')
        .replace(/&#039;/g, "'");
}

function nochange() {
    $('#firstChangePassDiv').hide();
    $('#Reader').css("position", "inherit");
}

// Create the json required from the ox backend
function createJson(to, from, subject, content, origMail, format) {
    var attachments = {
            content : content,
            content_type : (format === 'html' ? "text/html" : "text/plain")
    };
    var headers = {"X-OxGuard-Expiration" : 0, "X-OxGuard-ExtraPass" : ""};
       if (origMail.headers['Message-ID'] !== undefined) {
            headers['In-Reply-To'] = origMail.headers['Message-ID'];
            if (origMail.headers.References !== undefined) {
                headers['References']= (origMail.headers['References'] + origMail.headers['Message-ID']);
            } else {
                headers['References'] = origMail.headers['Message-ID'];
            }
        }
    var data = {
            from : [from],
            to : to,
            cc : [],
            bcc : [],
            headers : headers,
            reply_to : from,
            subject : subject,
            priority : 3,
            vcard : 0,
            attachments : [attachments],
            nested_msgs : [],
            sendtype: 0,
            contact_ids: [],
            infostore_ids: []
    };

    var json = {
            data : data,
            files : [],
            format : "html",
            mode : "compose",
            signature: ""
    };
    log('returnJson', json);
    return (json);

}

function forgotPass() {
    var email = $('#EmailAddr').val();
    if (email.indexOf("@") >= 1) {
        $('#wait').show();
        var url = root + 'oxguard/guest?action=getquestion&email=' + email;
        $.get(url, function (data) {
            log ('Reset get question ', data);
            if (data.trim() === "nf") {
                $('#EmailAddr').focus();
                $('#ForgotError').html(i18n.t("Please provide valid email address"));
                $('#ForgotError').show();
                $('#wait').hide();
                return;
            }
            if ((data.indexOf("default") > -1) || (data === "")) {
                $('#AskForgotQuestion').text(i18n.t("Favorite Movie"));
            } else {
                $('#AskForgotQuestion').text(data);
            }
            $('#wait').hide();
            if (data.trim() === 'nr') {
                //$('#ForgotError').html(i18n.t("Password reset not available for this account"));
                //$('#ForgotError').show();
                view('Reset');
                return;
            }
            if ((data.trim() === "null") || (data.trim() === "PIN")) {// If no recovery answer yet set up, reset
                forgotPass2()
                $('#wait').show();
            } else {
                view ('ForgotPage2');
            }
        })
        .fail (function () {
            $('#ForgotError').html(i18n.t("Failed to connect to server"));
        $('#ForgotError').show();
        });

    } else {
        $('#EmailAddr').focus();
        $('#ForgotError').html(i18n.t("Please provide valid email address"));
        $('#ForgotError').show();
    }

}

function forgotPass2() {
    var email = $('#EmailAddr').val();
    var answer = $('#forgotAnswer').val();
    $('#ForgotError2').hide();
    $('#wait2').show();
    var url = root + 'oxguard/guest?action=resetpassword&email=' + email + '&answer=' + answer + '&language=' + language + '&templid=' + templid;
    $.get(url, function (data){
        log("forgot2", data);
        $('#wait2').hide();
        if (data.indexOf('OK') > -1) {
            alert (i18n.t("Password Emailed"));
            view ('login');
        }
        if (data.indexOf('FailNotify') > -1) { 
            alert (i18n.t("Failed to change password"));
            view ('login');
        }
        if (data.trim() === 'No Match') {
            $('#ForgotError2').html(i18n.t("Incorrect Answer"));
            $('#ForgotError2').show();
        }
        if (data.trim() === 'lockout') {
            $('#ForgotError2').html(i18n.t("Temporary account lockout"));
            $('#ForgotError2').show();
        }
    }).fail (function (data) {
        $('#wait2').hide();
        $('#wait').hide();
        log("Forgot2 fail ", data);
        if (data.responseText.indexOf('no recovery') > 0) {
            $('#ForgotError2').html(i18n.t("Password reset not available for this account"));
        } else {
            $('#ForgotError2').html(i18n.t("Failed to change password"));//Error Generate Here
        }
        $('#ForgotError2').show();
    });
}

// Function to send account reset request
function resetAccount () {
    view('ResetResultDiv');
    var email = $('#EmailAddr').val();
    var url = root + 'oxguard/guest?action=resetaccount&user=' + email + '&templid=' + templid;
    $.get(url, function (data) {
        if (data instanceof Array) data = data[0];
        log("reset", data);
        if (data && data.error) {
            $('#ResetResult').text(data.error + ' ' + data.error_desc);
        } else {
            if (data.data) {
                $('#ResetResult').html(i18n.t("A verification email has been sent  Please check your inbox and follow the instructions in the email"));
            } else {
                $('#ResetResult').text(i18n.t("Unkown Error"));
            }
        }
    });
}

function doResetAccount() {
    var pass1 = $('#resetnew1').val();
    var pass2 = $('#resetnew2').val();
    if (pass1 != pass2) {
        $('#resetError').text(i18n.t('Passwords not equal'));
        return;
    }
    if (pass1.length < 4) {
        $('#resetError').text(i18n.t('Passwords too short'));
    }
    $('#resetError').text('');
    var resetid = getURLParameter('resetid');
    var email = getURLParameter('user');
    var data = {
            email: email,
            resetid: resetid,
            password: pass1
    }
    var url = root + 'oxguard/guest?action=doresetaccount';
       $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(data),
            contentType:'application/json; charset=utf-8',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function(data){
                if (data instanceof Array) data = data[0];
                if (data && data.error) {
                    $('#resetErrorDiv').show();
                    $('#resetErrorBox').text(data.error);
                } else {
                    $('#resetErrorDiv').hide();
                    $('#ResetStep1').hide();
                    $('#ResetStep2').show();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
       });
}

function view (element) {
    $('.view').hide();
    if (element !== undefined)
        $('#' + element).show();

}

function back () {
    view('Reader');
}

// Convert HTML email to plaintext
function convert (html, start) {
    try {
        if (start === undefined)
            start = 0;
        var i = html.indexOf('<blockquote', start);
        if (i > -1) {
            i = html.indexOf('>', i) + 1;
            html = convert(html, i);
        }
        var j = html.indexOf('</blockquote');
        var substring = html.substring(start);
        if (j > -1) {
            substring = html.substring(start, j);
        }
        var repl = '\n';
        if (i > -1) repl = '\n> ';
        var newstring = substring.replace(/\n/g, repl).replace(/<br[ \/]*>/gi, repl)
                .replace(/<p[\/]*>/gi, repl).replace(/<blockquote[^>]*>/,repl).replace(/<(?:.|\s)*?>/g, '') + repl;
        html = html.replace(substring, newstring);
        return (html);
    } catch (e) {
        console.log(e);
        return (html);
    }
}

function convertHTML(html) {
    return (' \n' + convert(html, 0).replace(/<blockquote[^>]*>/,'\n ').replace(/<(?:.|\s)*?>/g, ''));
}

function convertPlain (plain) {
    if (plain.indexOf(' \n') === 0) {  // If starts withour padding at top, remove
        return (plain.substring(3).replace(/\n/g, '<br>').replace(/>\n/, '<br>'));
    }
    return (plain.replace(/\n/g, '<br>').replace(/>\n/, '<br>'));
}

////////  Section for re-uploading email attachments if the cache has expired

function changeUploadList(uploads) {
    for (var i=0; i<uploads.files.length; i++) {
        uploadlist.push(uploads.files[i]);
    }
    $(uploads).val('');  // Clear file list
    drawUploads();
}

//Draw upload list
function drawUploads () {
    var list = '';
    $('.deleteAttach').unbind();
    for (var i=0; i< uploadlist.length; i++) {
        if (i === 0) {
            list = createLink(uploadlist[i]);
        } else {
            list = list + createLink(uploadlist[i]);
        }
    }

    $('#uploadList').html($('<ul>').append(list));
    $('.deleteAttach').click(function (e) {
        deleteUpload($(e.target).parent().attr('data'));
        drawUploads ();
    });
}

function deleteUpload (name) {
    for (var i =0; i < uploadlist.length; i++) {
        if (uploadlist[i] !== undefined) {
            if (uploadlist[i].name.trim() === name.trim()) {
                delete uploadlist[i];
                return;
            }
        }
    }
}

function uploadEmail() {
    if (uploadlist.length > 0) {
        var formData = new FormData();
        log('To upload', uploadlist);
        for (var l = 0; l < uploadlist.length; l++) {
            if (uploadlist[l] !== undefined)
                formData.append('file' + l, uploadlist[l]);
        }
    } else {
        alert (i18n.t('No files selected'));
        return;
    }
    // Reset upload bar
    $('#uploadProgress').attr('aria-valuenow', 0)
    .css('width', '0%');
    $('#uploading').show();
    var url = root + 'oxguard/guest?action=upload' + '&itemid=' + itemid +
    '&session=' + sessionID + '&userid=' + userid + '&cid=' + cid + "&auth=" + encodeURIComponent(auth);
    $.ajax({
        url: url,
        type: "POST",
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,   // tell jQuery not to set contentType
        xhr: function() {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress',function(ev) {
                    if (ev.lengthComputable) {
                        var percentComplete = Math.floor(ev.loaded * 100 / ev.total);
                        $('#uploadProgress').attr('aria-valuenow', percentComplete)
                        .css('width', percentComplete + '%');
                        log('Uploaded '+percentComplete+'%');
                        // update UI to reflect percentUploaded
                    }
               }, false);
            }
            return myXhr;
        },
        success: function(data){
            $('#uploading').hide();
            okLogin(savedata);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            log(XMLHttpRequest);
            $('#uploading').hide();
            switch (XMLHttpRequest.responseText.trim()) {
            case "Not valid Guard files":
                alert (i18n.t('Not a valid encrypted file  Look for file with suffix of grd'));
                return;
            case "not decodable":
                alert (i18n.t('Cannot decode that file  Correct password?  Correct Guard system?'));
                return;
            case "Storage failure":
                alert (i18n.t('The server was unable to store that file'));
                return;
            default:
                alert(i18n.t('Problem uploading the file'));
                return;
            }
        }
    });
}

// PGP Management

function pgpManage() {
    view('pgpManage');
    updateCheckBox();
}

function downloadPGPPublic () {
        var params = '&id=' + userid + '&cid=' + cid + '&auth=' + encodeURIComponent(auth);
        window.open(root + '/oxguard/pgpmail/public.asc?action=getkeybyid' + params, '_blank');
}

function downloadPGPPrivate () {
    var params = '&id=' + userid + '&cid=' + cid + '&auth=' + encodeURIComponent(auth) + '&guest=true&both=true';
    var link = root + '/oxguard/pgp/private.asc?action=getprivkeybyid' + params;
    window.open(link, '_blank');
}

function updateCheckBox () {
    if (settings.inline) {
        $('#inline').prop('checked', true);
    } else $('#inline').prop('checked', false);
    if (settings.direct) {
        $('#direct').prop('checked', true);
        $('#inlineDiv').show();
    } else {
        $('#direct').prop('checked', false);
        $('#inlineDiv').hide();
    }
}

function showPGPDirect () {
    if ($('#direct').is('::checked') !== true) {
        $('.mask').show();
        $('.pgpDirect').show();
        return false;
    }
}

function setDirect() {
    $('#direct').prop('checked', true);
    updateSettings();
    $('.mask').hide();
    $('.pgpDirect').hide();
}

function updateSettings () {
    var inline = $('#inline').is('::checked');
    var direct = $('#direct').is('::checked');
    var data = {
            inline : inline,
            direct : direct
    };
    settings.inline = inline;
    settings.direct = direct;

    if (!direct) {
        $('#inlineDiv').hide(); // hide inline data if not direct
    } else $('#inlineDiv').show();

    var json = {
            id : userid,
            cid : cid,
            data : data
    };
    $.ajax({
        url: root + '/oxguard/settings?action=set&session=' + sessionID + '&auth=' + encodeURIComponent(auth),
        type:'POST',
        data: JSON.stringify(json),
        contentType:'application/json; charset=utf-8',
        success: function () {
            log('updated settings');
        }
    })
    .fail (function () {
        alert (i18n.t('Failed to update settings'))
    });
}

function rKeys (data) {
    var keys = data.keys;
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var tr = $('<tr>');
        var td1 = $('<td>');
        var select = $('<input type="checkbox" class="keycheck" id="key' + i + '" value="' + i + '">');
        td1.append(select);
        var td2 = $('<td>');
        var ids = key.ids;
        for (var j = 0; j < ids.length; j++) {
            td2.append(ids[j].replace('<', '&lt').replace('>', '&gt') + '<br>');
        }
        var td3 = $('<td>');
        var fingerprints = key.fingerprints;
        for (var k = 0; k < fingerprints.length; k++) {
            td3.append(fingerprints[k] + '<br>');
        }
        var td4 = $('<td>');
        var pass = $('<input type="password" id="pass' + i + '">');
        td4.append(pass);
        tr.append(td1).append(td2).append(td3).append(td4);
        $('#keytable').append(tr);
    }
    view('listkeys');
}

function downloadKeys() {
    var keys = [];
    $('.keycheck').each(function (e,v) {
       if ($(v).is(':checked')) {
           var index = $(v).val();
           key = {
                   username : username,
                   index : index,
                   password : $('#pass' + index).val()
           }
           keys.push(key);
       };
    });
    if (keys.length === 0) {
        alert(i18n.t('No keys selected'));
        return;
    }
    $('#keys').html(i18n.t('Click to download') + '<br><br>');
    for (var i = 0; i < keys.length; i++) {
        $.ajax({
            url: root + '/oxguard/retrieve?action=getkeys&session=' + sessionID + '&auth=' + encodeURIComponent(auth),
            type:'POST',
            data: JSON.stringify(keys[i]),
            contentType:'application/json; charset=utf-8',
            success: function (data) {
                var filename = i18n.t('Key');
                if (data.keydata.ids.length > 0) {
                    filename = data.keydata.ids[0];
                }
                var a = $('<a href="data:application/pgp;charset=utf-8,' + data.data + '" download="' + filename + '.asc" class="downloadkey">' + filename.replace('<','&lt').replace('>','&gt') + '</a>');
                $('#keys').append(a).append('<br><br>');
                $('#keys').show();
            }
        })
        .fail (function (err) {
            if (err.responseText.trim() === 'bad password') {
                alert(i18n.t('Bad Password'));
                return;
            }
            if (err.responseText.trim() === 'lockout') {
                alert(i18n.t('Account locked out try later'));
                return;
            }
            alert(i18n.t('Fail'));
            console.log(err);
        });
    }


}

function logOut () {
    $.get(root + '/oxguard/login?action=logout&auth=' + encodeURIComponent(auth))
    .done(function () {
        view('logout');
    })
    .fail(function () {
        alert(i18n.t('Fail'));
    });

}

function log (where, data) {
    if (!debug) return;
    if (typeof console === "undefined" || typeof console.log === "undefined") return;// Close if IE dev tools not open
    console.log(where);
    console.log(data);
}

var debug = (getURLParameter("debug") === "1");




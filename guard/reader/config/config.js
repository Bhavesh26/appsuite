// Use this file to configure the configuration settings for the Guest reader
// Only needed if you are not hosting the reader under default reader installation
// I.E. if the url to the reader is https://somedomain.com/guard/reader, then you don't need this file

// Set the return value of getRoot to the base url for oxguard (up to appsuite/api)
var rootPath = '/appsuite/api/';

//Set this value to a fallback template ID if the user comes in to the reader without using
//An email link.
var defaultId = 0;


////////// No Need to modify below this line

function getRoot () {
    return rootPath;
}

function getDefaultTemplateId () {
	return (defaultId);
}

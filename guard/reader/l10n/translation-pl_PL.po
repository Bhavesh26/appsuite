# Jolanta <jolanta@bureau-cornavin.com>, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: OX Guard translation\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-10-31 11:48+0100\n"
"Last-Translator: Greg Hill <greg@docgreg.com>\n"
"Language-Team: Polish <>\n"
"Language: pl_PL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 1.8.9\n"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:511
msgid "Add Attachment"
msgstr "Dodaj załącznik"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1262
msgid ""
"A verification email has been sent  Please check your inbox and follow the "
"instructions in the email"
msgstr "Wysłaliśmy weryfikacyjną wiadomość e-mail. Zajrzyj do skrzynki odbiorczej i postępuj zgodnie z instrukcjami z wiadomości."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:418
msgid "Please replace the temporary password with a new password"
msgstr "Zastąp hasło tymczasowe nowym hasłem"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:536
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:545
msgid "Sending"
msgstr "Wysyłanie"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1457
msgid "Problem uploading the file"
msgstr "Problem z przesłaniem pliku"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:565
msgid "Retrieve"
msgstr "Pobierz"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:229
msgid "Old"
msgstr "Stare"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:277
msgid "The sender required a pin for viewing this email  Please enter it now"
msgstr "Aby wyświetlić tę wiadomość e-mail, należy wpisać kod PIN. Wpisz ten kod."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1587
msgid "Click to download"
msgstr "Kliknij, aby pobrać"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1409
msgid "No files selected"
msgstr "Nie wybrano plików"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1230
msgid "Incorrect Answer"
msgstr "Odpowiedź niepoprawna"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:50
msgid "To read this encrypted email please provide your credentials first"
msgstr "Aby przeczytać tę zaszyfrowaną wiadomość e-mail, podaj swoje poświadczenia"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:469
msgid "Download Private Key"
msgstr "Pobierz klucz prywatny"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:551
msgid "Your PGP Private Keys"
msgstr "Twoje klucze prywatne PGP"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:588
msgid "You have been logged out"
msgstr "Nastąpiło wylogowanie"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1187
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1243
msgid "Password reset not available for this account"
msgstr "Zresetowanie hasła do tego konta nie jest możliwe"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:458
msgid "Do not use this reader for PGP emails  I will use my own PGP client"
msgstr "Nie używaj tego czytnika do wiadomości e-mail PGP Użyję własnego klienta PGP"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:153
msgid ""
"This email requires an additional password that was assigned by the sender  "
"Please enter it now"
msgstr "Ta wiadomość e-mail wymaga podania dodatkowego hasła przypisanego przez nadawcę. Wprowadź hasło."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:162
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:191
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:293
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:454
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:575
msgid "Back"
msgstr "Wstecz"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:226
msgid "Please choose a new password"
msgstr "Wybierz nowe hasło"

#. <!--   4 digit pin is required to open -->
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:93
msgid "PIN Required"
msgstr "Wymagany kod PIN"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1613
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1627
msgid "Fail"
msgstr "Błąd"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:680
msgid "This Email was signed and failed verification"
msgstr "Wiadomość e-mail została podpisana, lecz nie została zweryfikowana"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:300
msgid ""
"This will not delete the old account, rather it will set up a new set of "
"encryption keys for future emails"
msgstr "Nie spowoduje to usunięcia poprzedniego konta, a jedynie założenie nowych kluczy do szyfrowania kolejnych wiadomości."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:257
msgid ""
"All emails sent using this service are password protected  Please enter a "
"new password for accessing any email sent to you using this service"
msgstr "Wszystkie wiadomości e-mail wysyłane za pomocą tej usługi są zabezpieczone hasłem. Wpisz nowe hasło, które zabezpieczy wiadomości wysyłane z tej usługi."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:218
msgid "Welcome to $s"
msgstr "$s — witamy"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:301
msgid ""
"If you remember your old password, you will still be able to login and read "
"your old emails  However, any new emails will be sent to the new account"
msgstr "Jeśli pamiętasz poprzednie hasło, możesz się zalogować na poprzednie konto i odczytać starsze wiadomości. Wszystkie kolejne wiadomości zostaną wysłane na nowe konto."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:340
msgid "Create New"
msgstr "Utwórz nowe"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:61
msgid "Login"
msgstr "Zaloguj"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:68
msgid "Please provide your credentials"
msgstr "Podaj poświadczenia"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:254
msgid "This appears to be the first time you have used this site"
msgstr "Wygląda na to, że używasz tej witryny po raz pierwszy."

#. For the prefix of the subject reply I.E. Re: The message you sent
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:986
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1070
msgid "re"
msgstr "odp."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1042
msgid ""
"The authorization to send a reply was not valid  Did you have another reader "
"open?  Please reload and log in again"
msgstr "Uwierzytelnienie do wysłania odpowiedzi nie było prawidłowe. Czy nie ma otwartego innego czytnika? Przeładuj i zaloguj się ponownie"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1454
msgid "The server was unable to store that file"
msgstr "Serwer nie mógł zapisać tego pliku"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:79
msgid "Authorizing"
msgstr "Autoryzowanie"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1595
msgid "Key"
msgstr "Klucz"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:483
msgid "Please provide an answer for the security question"
msgstr "Odpowiedz na pytanie zabezpieczające"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:462
msgid "Send using PGP Inline"
msgstr "Wyślij przy użyciu PGP Inline"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:557
msgid "Fingerprints"
msgstr "Odciski"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:239
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:436
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:489
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:533
msgid "Cancel"
msgstr "Anuluj"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:140
msgid "Upload"
msgstr "Prześlij"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:235
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:264
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:331
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:424
msgid "Confirm"
msgstr "Potwierdź"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:380
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:406
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:452
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:573
msgid "Log Out"
msgstr "Wyloguj"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:178
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:214
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:353
msgid "Please wait"
msgstr "Czekaj"

#. <!--   Password Reset Header -->
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:165
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:193
msgid "Password Reset"
msgstr "Reset hasła"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:300
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:306
msgid "Error checking PIN"
msgstr "Błąd podczas sprawdzania kodu PIN"

#. Bad username or Password at login
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:181
msgid "Bad username / Password"
msgstr "Nieprawidłowa nazwa użytkownika lub hasło"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:467
msgid "Download Public Key"
msgstr "Pobierz klucz publiczny"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:197
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:294
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1610
msgid "Account locked out try later"
msgstr "Konto zablokowane. Spróbuj później."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:448
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:455
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:525
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1226
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1245
msgid "Failed to change password"
msgstr "Nie udało się zmienić hasła"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:484
msgid ""
"You must also ensure that you download the public and private keys using the "
"links shown on this page, and configure your email client to use them to "
"decrypt and sign emails"
msgstr "Upewnij się także, czy publiczne i prywatne klucze pobrano za pomocą łączy na tej stronie oraz skonfiguruj w kliencie poczty e-mail funkcję odszyfrowywania i podpisywania wiadomości."

#. <!--   Text only formatting for email reply-->
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:527
msgid "Text"
msgstr "Tekst"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1278
msgid "Passwords too short"
msgstr "Hasło jest za krótkie"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:97
msgid "Pin"
msgstr "Kod PIN"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:279
msgid "PIN"
msgstr "Kod PIN"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:323
msgid "You are about to reset your account and choose a new security password"
msgstr "Chcesz zresetować konto i wprowadzić nowe hasło"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:315
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1274
msgid "Passwords not equal"
msgstr "Hasła są różne"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:137
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:555
msgid "Select"
msgstr "Wybierz"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:377
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:403
msgid "Reply"
msgstr "Odpowiedz"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1539
msgid "Failed to update settings"
msgstr "Nie udało się zaktualizować ustawień"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:383
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:409
msgid "Change Password"
msgstr "Zmień hasło"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:269
msgid ""
"Please type in a security question and answer in case you forget your "
"password"
msgstr "Na wypadek jeśli zapomnisz hasła, wpisz pytanie zabezpieczające i odpowiedź"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:581
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:396
msgid "Cc"
msgstr "DW"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1606
msgid "Bad Password"
msgstr "Nieprawidłowe hasło"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:232
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:260
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:327
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:421
msgid "New"
msgstr "Nowe"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:324
msgid ""
"The new password will apply to future secured emails received in this account"
msgstr "Nowe hasło będzie dotyczyło dalszych zabezpieczonych wiadomości odebranych na to konto"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:846
msgid "Attachment"
msgstr "Załącznik"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1451
msgid "Cannot decode that file  Correct password?  Correct Guard system?"
msgstr "Nie można odkodować pliku. Prawidłowe hasło? Właściwy system Guard?"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:299
msgid ""
"You can, however, reset the account so any future emails can be opened with "
"a new password"
msgstr "Możesz jednak zresetować konto, co sprawi, że przyszłe wiadomości będą otwierane za pomocą nowego hasła."

#. <!--   HTML formatting for reply -->
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:529
msgid "HTML"
msgstr "HTML"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:343
msgid "Your account has been reset"
msgstr "Konto zostało zresetowane"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:386
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:412
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:456
msgid "Advanced User Options"
msgstr "Zaawansowane opcje użytkownika"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:532
msgid "Send"
msgstr "Wyślij"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1584
msgid "No keys selected"
msgstr "Nie wybrano kluczy"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:403
msgid "Too short"
msgstr "Zbyt krótkie"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:577
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:395
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:503
msgid "To"
msgstr "Do"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:82
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:110
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:113
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:181
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:217
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:312
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:356
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:367
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:370
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:539
msgid "Please Wait"
msgstr "Czekaj"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:303
msgid "Reset"
msgstr "Resetuj"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:575
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:393
msgid "Sent"
msgstr "Wysłano"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:66
msgid "Forgot Password"
msgstr "Nie pamiętam hasła"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:472
msgid "Password must be at least 8 characters long"
msgstr "Hasło musi się składać z co najmniej 8 znaków"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1222
msgid "Password Emailed"
msgstr "Hasło wysłane pocztą"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1044
msgid "An error occurred during sending the encrypted email reply"
msgstr "Podczas wysyłania zaszyfrowanej odpowiedzi e-mail wystąpił błąd."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1181
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:271
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:430
msgid "Favorite Movie"
msgstr "Ulubiony film"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:324
msgid "Answer required for password reset option"
msgstr "W celu zresetowania hasła konieczne jest wpisanie odpowiedzi"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:378
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:404
msgid "Reply All"
msgstr "Odpowiedz wszystkim"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:690
msgid "An error occurred"
msgstr "Wystąpił błąd"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:357
msgid "Password Length Too Short"
msgstr "Hasło jest za krótkie"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:296
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:309
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:321
msgid "Account Reset"
msgstr "Zresetuj konto"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:657
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:660
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:120
msgid "This email is no longer available"
msgstr "Wiadomość e-mail nie jest już dostępna"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:677
msgid "This Email was signed and verified"
msgstr "Wiadomość e-mail została podpisana i zweryfikowana"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:556
msgid "IDs"
msgstr "Identyfikatory"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1032
msgid "Encrypted email reply has been sent successfully"
msgstr "Pomyślnie wysłano zaszyfrowaną odpowiedź e-mail"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:126
msgid ""
"Please save the attachments to your disk drive, then click the button below "
"to re-upload the files"
msgstr "Zapisz załączniki na dysku twardym, a następnie kliknij poniższy przycisk, aby ponownie przesłać pliki"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:479
msgid ""
"This option, only for very advanced users, enables you to use your own PGP "
"client instead of the Guard reader"
msgstr "Ta opcja — proponowana tylko dla bardzo zaawansowanych użytkowników — pozwala na użycie własnego klienta PGP zamiast klienta z aplikacji Guard."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:123
msgid ""
"In order to read this email, you will need to upload the attachments that "
"were sent along with the original email"
msgstr "Aby odczytać tę wiadomość, należy przesłać załączniki, które zostały wysłane razem z oryginalną wiadomością e-mail"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:477
msgid "PGP Emails"
msgstr "Wiadomości z szyfrowaniem PGP"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:54
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:169
msgid "E-mail address"
msgstr "Adres e-mail"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:289
msgid "Bad PIN"
msgstr "Nieprawidłowy kod PIN"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:351
msgid "PIN invalid or missing"
msgstr "Brak kodu PIN lub kod jest nieprawidłowy"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1200
msgid "Failed to connect to server"
msgstr "Nie udało się połączyć z serwerem"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:270
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:429
msgid "Security question"
msgstr "Pytanie zabezpieczające"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1084
msgid "Original message from"
msgstr "Oryginalna wiadomość od:"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:558
msgid "Password if different"
msgstr "Hasło jest inne"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:197
msgid ""
"For verification, please answer the following question that was established "
"during your account setup"
msgstr "W celu weryfikacji proszę odpowiedz na poniższe pytania wprowadzone podczas konfiguracji konta"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:100
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:488
msgid "OK"
msgstr "OK"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1175
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1206
msgid "Please provide valid email address"
msgstr "Podaj prawidłowy adres e-mail"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:58
msgid "Password"
msgstr "Hasło"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:298
msgid "This account does not have a way to recover lost passwords"
msgstr "To konto nie oferuje sposobu na przywracanie utraconych haseł"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:439
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:513
msgid "Password Changed"
msgstr "Hasło zostało zmienione"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:155
msgid "Additional password"
msgstr "Dodatkowe hasło"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:94
msgid "The sender has assigned a PIN to this email  Please enter it below"
msgstr "Nadawca przypisał kod PIN do tej wiadomości e-mail. Wpisz go poniżej."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:398
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:414
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:477
msgid "Passwords do not match"
msgstr "Hasła nie są zgodne"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1264
msgid "Unkown Error"
msgstr "Nieznany błąd"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:372
msgid "Problem connecting with the server"
msgstr "Wystąpił błąd podczas próby połączenia się z serwerem"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:587
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:394
msgid "From"
msgstr "Od"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1234
msgid "Temporary account lockout"
msgstr "Tymczasowa blokada konta"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:273
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:432
msgid "Answer"
msgstr "Odpowiedź"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:428
msgid "Please type in a security question and answer"
msgstr "Wpisz pytanie zabezpieczające i odpowiedź"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:1448
msgid "Not a valid encrypted file  Look for file with suffix of grd"
msgstr "Nieprawidłowy zaszyfrowany plik. Znajdź plik z sufiksem grd."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:482
msgid ""
"In order to do so, you must have an email client capable of encrypting and "
"decrypting PGP emails"
msgstr "Aby to zrobić, potrzebujesz klienta e-mail umożliwiającego szyfrowanie i odszyfrowywanie wiadomości zakodowanych za pomocą standardu PGP."

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:211
msgid "Account not found"
msgstr "Nie znaleziono konta"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:156
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:171
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:207
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:238
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:284
#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/reader.html:435
msgid "Ok"
msgstr "Ok"

#: /home/greg/git/guard/com.openexchange.guard.translation/build/../../guest-reader/scripts/reader.js:90
msgid "Please provide your encryption password"
msgstr "Podaj hasło szyfrowania"

#~ msgid "PGP Management"
#~ msgstr "Zarządzanie PGP"

#~ msgid "Please answer the question you asked earlier"
#~ msgstr "Odpowiedz na wcześniej zadane pytanie"

#~ msgid "PGP Management Options (Advanced Users)"
#~ msgstr "Opcje zarządzania PGP (użytkownicy zaawansowani)"
